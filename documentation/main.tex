\documentclass[a4paper, twoside, 10pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{algorithm,algpseudocode}
\usepackage{a4wide}
\usepackage[french]{babel}
\selectlanguage{french}
\usepackage[T1]{fontenc}
\usepackage[section]{placeins}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{caption}
\usepackage{subcaption}
\usepackage[hidelinks]{hyperref}
\usepackage[colorinlistoftodos]{todonotes}
\usepackage{listings}
\usepackage{color}
\usepackage{float}
\usepackage{multirow}
\usepackage[normalem]{ulem}
\usepackage{adjustbox}

\newcommand{\argmax}[1]{\underset{#1}{\operatorname{arg}\,\operatorname{max}}\;}

\title{Projet FoSyMa - Groupe 2}
\author{Pierre DUBAILLAY, Tarek CHOUAKI}
\begin{document}
\maketitle

\pagenumbering{gobble}
\clearpage
\paragraph{}
\newpage
\tableofcontents
\clearpage

\pagenumbering{arabic}
\section{Introduction}
\paragraph{}Le but de ce projet est de développer une version multi-agent d'un jeu inspiré de \og Hunt the Wumpus \fg{}. Dans cette variante, les agents doivent explorer le labyrinthe pour y collecter le maximum de trésors. Trois types d'agents, en plus du Wumpus, seront déployés :
\begin{itemize}
	\item des agents explorateurs, qui ne peuvent ni stocker les trésors, ni les ramasser
    \item les agents silo, qui ne peuvent que stocker les trésors
    \item des agents collecteurs, dont le rôle est de collecter un type de trésor (différent selon les agents) et de les livrer un agent silo
\end{itemize}
L'environnement étant inconnu au départ, l'objectif des agents est donc d'explorer le graphe à la recherche des trésors. Les agents doivent communiquer entre eux, aussi il est nécessaire de développer des protocoles de communication.

\paragraph{}Dans un premier temps, nous aborderons les différentes stratégies mises en place pour les différents agents. Nous présenterons ensuite les protocoles de communication implémentés en explicitant une séquence typique d'utilisation et en donnant une complexité au pire cas. Nous terminerons par quelques algorithmes notoires utilisés pour le calcul des trajectoires des agents.

\paragraph{}Nous nous sommes donnés pour objectif de limiter au maximum le nombre de messages envoyés ainsi que leur taille lorsque c'était possible. Aussi, seuls deux protocoles de communications ont été implantés, tous utilisant un nombre raisonnable de messages.

\section{Stratégies suivies par les agents}
\paragraph{}Les stratégies présentées dans cette section sont les stratégies générales des différents agents. Aussi la gestion des inter-blocages n'est pas incluse dans les algorithmes présentés. La résolution de ces derniers sera expliquée lorsque le protocole de résolution des inter-blocages sera abordé dans une prochaine section.

\subsection{Comportement général des agents}
\paragraph{}Chaque type d'agent développé possède une stratégie principale, responsable de la création et de l'exécution du plan de l'agent. Cette stratégie peut d'une part calculer l'ensemble des cibles admissibles pour l'agent, et d'autre part, générer un plan pour l'agent pour atteindre la cible. Ce plan est une suite de comportements à effectuer sur chaque nœud du graphe sur le chemin vers la cible.
\paragraph{}L'avantage de cette approche est que les calculs coûteux d'identification et de génération de plan ne sont effectués qu'une seule fois (uniquement lorsqu'on doit recalculer une nouvelle cible). Aussi, la plupart du temps, nos agents ne font qu'exécuter bêtement leur plan. Néanmoins, il existe des cas dans lesquels le plan ne se déroule pas comme prévu : inter-blocage, wumpus, échange de graphes qui rendent la cible obsolète ... Dans toutes ces situations, il faut que le plan de l'agent soit révisé. Nous traiterons le cas des inter-blocages dans une partie dédiée. Nous allons traiter le cas simple où une mise à jour reçue d'un autre agent rend obsolète la cible actuelle.
\paragraph{}Dans ce cas de figure, la stratégie de l'agent doit refaire un cycle de planification qui tient compte des mises à jours. Pour que ces changements soient détectés, nous maintenons une date de dernière modification. Si cette date est plus récente que celle du dernier calcul de cibles, l'algorithme relance un cycle de planification et écrase le plan précédent. L'avantage de cette technique est que le plan de l'agent s'adapte en permanence à l'état des connaissances. Nous y gagnons en flexibilité et évitons ainsi des visites redondantes.

\subsection{Agent explorateur}
\paragraph{} Les agents explorateurs sont des agents dont le seul rôle est de découvrir la carte et de la communiquer, lorsque c'est possible, aux autres agents. La stratégie suivie par un agent explorateur est très simple :
\begin{itemize}
	\item tant que l'ensemble de l'environnement n'a pas été exploré, notre agent calcule un chemin vers le nœud non visité le plus proche. Une fois le chemin établi, il génère la suite des comportements et lance le plan
    \item lorsque tous les nœuds ont été explorés, l'agent explorateur va tenter de garder une carte la plus à jour possible en revisitant les nœuds dont les informations sont les plus vieilles. Une fois un tel nœud choisi, le plan est établi puis exécuté.
\end{itemize}
L'algorithme présenté ici ne prend pas en compte la révision de plan ou la résolution des inter-blocages par soucis de concision.

\begin{algorithm}[ht]
	\caption{Navigator Strategy}
	\begin{algorithmic}
        \While{true}
        	\If{there are remaining unvisited nodes}
            	\State get all unvisited nodes
            	\State use the dijkstra algorithm to get a path to the closest one
            \Else{}
            	\State get the oldest visited node
            	\State find a path with the dijkstra algorithm
            \EndIf{}
            \State create a plan with all behaviours the agent needs to execute on the path to the target
            \State execute the plan
        \EndWhile
  \end{algorithmic}
\end{algorithm}

\FloatBarrier
\subsection{Agent collecteur}
\paragraph{}Les agents collecteurs ont pour objectif de collecter le maximum de trésors de leur type (diamants ou trésor). Au début, la carte n'est pas connue. Les agents collecteurs se comportent alors comme des agents explorateurs pour découvrir des trésors de leur type. Lorsque de tels trésors sont trouvés, l'agent sélectionne le plus proche des deux meilleurs trésors, établit un plan vers le nœud et commence l'exécution. Une fois que l'agent collecteur doit vider son sac, deux cas de figure apparaissent : 
\begin{itemize}
	\item l'agent connaît toute la carte. Dans ce cas, l'agent calcule le point central et se rend dans un de ses voisins.
    \item l'agent ne connaît pas toute la carte. Dans ce cas, l'agent ne peut pas calculer le point central et va donc partir explorer les portions manquantes.
\end{itemize}
Une fois que les agents n'ont plus aucun trésors à ramasser, ils sont envoyés loin de l'agent silo pour laisser la place à d'éventuels autres collecteurs.

\begin{algorithm}[ht]
	\caption{GoldDigger Strategy}
	\begin{algorithmic}
        \While{true}
        	\If{if there is at least 5 units free in my backpack and we know where to find treasurs}
            	\State $target \leftarrow$ the closest best treasure
                 
            \Else{}
            	\If{The map is entirely discovered}
                	\State $target \leftarrow$ near the center of the graph
                \Else{}
                	\State $target \leftarrow$ an unvisited node
                \EndIf{}
            \EndIf{}
            \State create a plan with all behaviours the agent needs to execute on the path to the $target$
            \State execute the plan
        \EndWhile
  \end{algorithmic}
\end{algorithm}

\FloatBarrier
\subsection{Agent silo}
\paragraph{}L'agent silo stocke l'ensemble des trésors collectés par les agents collecteurs. Afin qu'il soit le plus près possible de tous les agents, l'objectif de la stratégie principale est de positionner l'agent silo sur un des centres du graphe (celui avec le plus grand degré). Pour atteindre l'objectif, l'agent silo explore le graphe jusqu'à en connaître la totalité. Il peut ainsi calculer le centre du graphe et s'y rendre. Une fois le centre du graphe, l'agent silo reste à l'écoute des autres agents pour échanger des informations ou résoudre des inter-blocages.
\paragraph{}L'avantage de chercher le centre du graphe est qu'une fois que tous les nœuds ont été visités, pour peu que tous les agents collecteurs exécutent le même algorithme, tous trouveront le même nœud. Il est donc naturel d'y positionner l'agent silo.

\begin{algorithm}[ht]
	\caption{Stewart Strategy}
	\begin{algorithmic}
        \While{there are remaining unvisited nodes}
        	\State get all unvisited nodes
            \State use the dijkstra algorithm to get a path to the closest one
            \State create a plan with all behaviours the agent needs to execute on the path to the target
            \State execute the plan
        \EndWhile
        \State compute the center of the graph
        \State create a plan to reach the center and execute it
  \end{algorithmic}
\end{algorithm}

\section{Protocoles de communication}
\paragraph{}Afin de mutualiser les informations que possèdent nos agents, mais aussi de résoudre les différentes situations d'inter-blocage, nos agents doivent communiquer entre eux. Pour ce faire, nous avons mis en place divers protocoles de communication. Avant d'entrer dans les détails de chaque protocole, nous allons discuter de la terminaison de ces derniers.

\subsection{Garantir la terminaison}
\paragraph{}Les limitations concernant les communications forcent, lorsque deux agents doivent communiquer, à s'arrêter pour rester à portée. Les agents ne s'exécutant pas à la même vitesse, il est possible qu'un agent reçoive un message d'un collègue et, le temps d'écrire la réponse, d'être hors de portée. Il est donc inconcevable pour l'agent auteur du premier message d'attendre indéfiniment la réponse de son interlocuteur. Pour garantir que, dans n'importe quelle situation, le protocole se terminera effectivement, nous imposons un délai de réponse lors des échanges. Un agent est considéré comme manquant si une réponse attendu n'est pas parvenue à temps. Aucune tentative ne sera tentée pour retrouver l'agent et le protocole sera tout simplement abandonné. Ainsi, nous sommes sûrs qu'aucun agent n'attendra indéfiniment et que tous les protocoles terminent.

\subsection{Échange des graphes}
\paragraph{}Lorsque deux agents se rencontrent, il est intéressant qu'ils partagent les informations dont ils disposent sur la topologie de l'environnement. Pour ce faire, nous avons mis en place un protocole d'échange des graphes.
\paragraph{}Lorsqu'un agent souhaite échanger ses informations avec des agents dans le secteur, ce dernier commence par découvrir les agents disponibles dans son rayon de communication. Pour ce faire, l'agent broadcast un message \textit{\og hello \fg{}} en fixant un \textit{Time-To-Live}. Au terme du temps spécifié, l'agent collecte tous les messages reçus, agrège les graphes avec le siens et répond à l'ensemble des autres agents avec le nouveau graphe. Concernant les erreurs, elles sont simplement ignorées. Par exemple, si un problème de sérialisation se produit sur l'un des messages, le graphe est ignoré mais une réponse sera tout de même apportée la jointure de tous les graphes ne contiendra pas le graphe manquant).

\paragraph{}Dans un première version du projet, les comportements de réception et d'envoi de graphe étaient exécutés sur chaque nœud. Étant donné notre objectif de limiter le nombre de messages échangés, cette première solution n'était pas envisageable. De plus, nous observions souvent des agents communiquant plusieurs fois de suite (lorsqu'après leur déplacement, ils étaient encore à porté). Nous avons donc décidé de limiter le comportement d'échange de graphe en imposant (arbitrairement) une certaine quantité d'actions réalisées entre deux échanges. 

\paragraph{Avantages}L'avantage de ce protocole est que d'une part nous ne broadcastons pas directement le graphe sur le réseau, et d'autre part, à la fin des échanges, tous les agents ont le graphe le plus à jour.

\subsubsection{Complexité}

\paragraph{Complexité pire des cas}Le protocole est on-ne-peut plus simple. L'agent initiateur, que nous nommerons $A$, broadcast un message de type \textit{INFORM} pour signaler sa présence. En posant $n$ le nombre d'agents dans le système, l'agent $A$ envoie $n-1$ messages. Dans le pire des cas, tous les agents répondent correctement avec leur graphe respectif. L'agent $A$ reçoit donc $n-1$. Une fois que ce dernier a agrégé l'ensemble des graphes, il envoie le résultat à tous les agents en communication soit $n-1$. Finalement, la complexité au pire cas est $O(n)$.

\paragraph{Complexité dans le meilleur des cas}Ce cas se produit lorsqu'aucun agent n'est à portée de l'agent $A$. Dans ce cas, le nombre de messages envoyé est donc $n-1$. La complexité dans le meilleur cas est $O(n)$.

\subsubsection{Optimalité}
\paragraph{En espace}Le protocole en lui même est sous-optimal concernant la complexité en espace : actuellement, une fois que l'agent $B$ a réalisé la jointure, il revoie l'intégralité de cette dernière plutôt que les informations qui n'ont pas été mises à jour dans son graphe (informations plus récentes et/ou nœuds inconnus de $A$). Aussi, si l'ordre de grandeur est correct, il est possible d'améliorer un peu la taille des messages.
\paragraph{En nombre de messages}Le protocole choisi fait un échange complet des graphes. Nous aurions pu diminuer le nombre de messages en ne faisant qu'envoyer le graphe de $A$ à tous les agents sans attendre de retour ni détecter les gents autour. L'inconvénient d'une telle méthode est que d'une part, la complexité en espace explose car le graphe entier est envoyé à tous les agents et que, d'autre part, l'agent $A$ ne reçoit aucune mise à jour des autres agents. Il faudra donc que les autres agents fassent de même pour que $A$ soit à jour (et donc envoyer $n-1$ fois le graphe, encore ...). Même dans un cadre d'échange de graphe, notre protocole est sous-optimal. Nous pourrions nous passer du message de ping (\textit{\og hello \fg{}} mais encore une fois, c'est un compromis avec la taille des messages échangés.

\begin{figure}[ht]
\centering\includegraphics[width=1\linewidth]{resources/SequenceEchangeDeGraphe.png}
\caption{Protocole d'échange de graphes entre les agents}
\end{figure}

\FloatBarrier
\subsection{Résolution des inter-blocages}
\subsubsection{Cas général}
\paragraph{}La résolution des inter-blocages se fait pair-à-pair entre deux agents. Le protocole de résolution s'enclenche lorsqu'un agent n'a pas réussi à se déplacer (MouveBehaviour). L'agent bloqué contacte les agents aux alentours pour initier le protocole de résolution d'inter-blocage avec un message contenant la position à laquelle il n'arrive pas à accéder ou consulte sa boîte aux lettres. Ce choix est fait de manière aléatoire. L'agent qui occupe cette position et reçoit le message d'initiation répond par un message contenant "\textit{me too}". L'agent ayant initié le protocole est alors l'agent maître (prioritaire) et l'agent ayant répondu est l'agent esclave (moins prioritaire).
\paragraph{}Lorsque la hiérarchie est établie, le maître envoie à son esclave son graphe. Ce dernier croise les informations reçues avec les siennes et calcule ses cibles. Une fois cette opération réalisée, l'esclave envoie au maître les informations à jour ainsi que la liste de ses cibles potentielles. Le maître met alors à jour son graphe et ses cibles. Il va alors tenter de calculer un chemin pour lui-même vers une de ses cibles, ainsi qu'un chemin pour son esclave. Bien évidement, le chemin de l'esclave ne doit pas passer par la position courante du maître. S'il n'existe pas de tel chemin, alors le maître va tenter de pousser l'esclave de son chemin vers une de ses cibles. Pour ce faire, il commence par calculer un chemin vers une de ses cibles. Une fois le chemin calculé, le maître remplace l'ensemble des cibles de l'esclave par tous les nœuds ne faisant pas partie du chemin. Il interdit également sa position courante et lance un calcul de plus court chemin, pour trouver, depuis la position de l'esclave, le nœud le plus proche hors de son chemin.

\paragraph{}Il peut arriver qu'il n'existe pas de nœud libre pour l'esclave. Si nous reprenons la situation précédente en inversant l'esclave et le maître ($B$ master et $A$ slave), il n'existe pas de tel nœud. Dans ce cas, en ultime recours, le maître va inverser les priorités. L'algorithme va de nouveau être déroulé, mais cette fois, l'esclave sera prioritaire.

\begin{figure}[ht]
\centering
\includegraphics[width=1\linewidth]{resources/master_slave.png}\caption{Protocole de résolution des inter-blocages}
\end{figure}

\begin{figure}[ht]
\centering
\includegraphics[width=1\linewidth]{resources/SequenceInterlock.png}\caption{Protocole de résolution des inter-blocages}
\end{figure}

\FloatBarrier
\subsubsection{Cas où l'agent bloqué ne parvient pas à communiquer}
\paragraph{}Il arrive qu'un agent bloqué ne puisse pas communiquer avec l'agent qui le bloque (notamment lorsque l'autre agent est le Wumpus). Dans ce cas particulier, nous déclenchons un comportement particulier (au bout de deux échecs du protocole régulier) : l'agent va tenter d'atteindre une autre de ses cible en interdisant la position de l'autre agent dans le chemin. Si cette solution n'aboutit pas, l'agent choisi un nœud au hasard et tente de s'y rendre pour revenir éventuellement plus tard.

\subsubsection{Cas particulier}
\paragraph{}Il existe une situation particulière qui doit être traitée différemment du cas général. Lorsque deux agents se trouvent dans un cul de sac, l'un voulant aller au fond et l'autre n'ayant pas de cible, l'agent sans cible ne va pas bouger au bout du processus de résolution. En effet, lorsque l'agent est prioritaire, n'ayant pas de cible il ne bougera pas. Et lorsqu'il est moins prioritaire, impossible de trouver un chemin l'amenant loin du chemin emprunté par l'autre agent. Dans ce cas particulier, nous devons donc faire reculer l'agent qui désire se rendre au fond de l'impasse jusqu'au premier nœud de degré supérieur ou égal à trois, le déplacer vers un des deux nœuds, faire avancer l'autre agent dans l'autre nœud pour libérer le passage.

\begin{figure}[ht]
\centering
\includegraphics[width=1\linewidth]{resources/no_target.png}\caption{Cas particulier: l'impasse}
\end{figure}

\FloatBarrier
\subsubsection{Complexité en temps}
\paragraph{}Dans le pire des cas, les deux agents entrent dans le même mode en même temps : initiateurs et où en attente. Pour pallier à ces problèmes, les TTLs sont aléatoires. Dans la suite de cette étude, nous omettrons ce cas dont la probabilité décroît avec le nombre d'essais. Pour en revenir à notre étude :
\begin{itemize}
	\item l'initiateur envoie un message à tous les agents. En posant $n$ le nombre d'agents, l'initiateur broadcast sa situation, envoyant ainsi $n-1$ messages. Le premier agent bloqué qui répond devient le l'interlocuteur privilégié de l'initiateur. Dans le pire des cas, tous les agents répondent, soit $n-1$ messages. S'en suit un échange pour débloquer la situation. Dans le pire des cas, nous devons aller au bout de l'échange.
    \item Suit un échange de graphe et de cibles, prenant 2 messages. Une fois que l'agent \og master \fg{} a calculé les chemins, il envoie celui de l'esclave à ce dernier. Cela prend $1$ message.
\end{itemize}
Au total, le protocole utilise $2*(n-1) + 3$ messages.

\subsubsection{Complexité en espace}
\paragraph{}Nous allons essayer de quantifier la quantité de données échangées lors de la résolution des inter-blocages. Nous allons compter une unité par nœud échangé. Lors de l'établissement de la communication entre le \og master \fg{}et le \og slave \fg{}, aucun nœud n'est échangé. Une fois cette dernière établie, l'agent \og master \fg{} envoie la totalité de son graphe à l'esclave. Si on note $n$ le nombre de nœuds dans le graphe, on a alors $n$ unités transmises du \og master\fg{} au \og slave \fg{}. Dans l'autre sens l'esclave transmet à son tour la totalité du graphe mis à jour, ainsi que l'ensemble de ses cibles. On peut borner le nombre de cibles par le nombre de nœuds dans le graphe, on a alors $2n$ unités transmises. Enfin, le maître envoie le chemin que doit suivre l'esclave qui encore une fois peut être borné par la taille du graphe, soit $n$ unités. On obtient une complexité en $O(n)$.

\subsubsection{Avantages et limitations}
\paragraph{Avantages}L'avantage principal de ce protocole est qu'il permet de résoudre la plupart des inter-blocages impliquant 2 agents.
\paragraph{Limitations}Le protocole actuel ne prend pas en charge les inter-blocages impliquant plusieurs agents. Lorsqu'une telle situation se produit, les agents ne communiquent qu'avec leur interlocuteur, sans prendre en compte la situation globale. Notre protocole ne permet pas non plus d'être sûr que l'interlocuteur d'un agent est bien l'agent avec lequel il est bloqué.

\section{Algorithmes}
\subsection{Calcul de chemins}

\paragraph{}
Le calcul des chemins se fait à l'aide de l'algorithme de Dijkstra. La variante implémentée prend en plus d'un nœud de départ et d'un ensemble de nœuds cibles un autre ensemble de nœuds à ignorer. Ce paramètre en plus nous permet de calculer des chemins de manière à résoudre des inter-blocages. 

\subsection{Centralité dans un graphe}

\paragraph{}
La question de centralité dans un graphe s'est posée dans le cadre de la détermination de la position de l'agent silo. Il fallait garantir la proximité des autres agents tout en évitant de rester sur un couloir ainsi que l'unicité de la position résultante du calcul des différents agents.\\
Pour cela, nous considérons d'abord les nœuds de degrés supérieur à deux, puis nous prenons les nœuds ayant la distance maximum aux autres nœuds la moins élevée (Cette distance est déterminée par un parcours en largeur). Ensuite, nous prenons les nœuds de degré maximum et enfin le premier nœud par ordre alphabétique sur les identifiants.
Ceci est détaillé dans les algorithmes ci-dessous.

\begin{algorithm}[ht]
\caption{GetFurthestDistance}
\begin{algorithmic}
\Require $root$ a vertex, $maxDepth > 0$
\Ensure $0 \leq result \leq maxDepth$
\State $L_0 = \{root\}$
\State $L = [L_0]$
\State $finished = False$
\State $currentDepth=0$
\While{not finished}
	\State $l \leftarrow last(L)$
    \State $newLevel \leftarrow \{v/ v\in V(v'), v'\in l\} \setminus \bigcup_{l' \in L}$
    \If{$newLevel \neq \emptyset$}
    	\State append($L$, $newLevel$)
        \State $currentDepth \leftarrow currentDepth+1$
        \State $finished \leftarrow (length(L) \geq maxDepth)$
    \Else
    	\State $finished \leftarrow True$
    \EndIf
\EndWhile
\State \Return $currentDepth$
\end{algorithmic}
\end{algorithm}


\begin{algorithm}[ht]
\caption{GetCentralPoint}
\begin{algorithmic}
\Require $G = (V, E)$ a graph 
\State $minDepth = +\infty$
\State $maxDegree = 0$
\State $P = \emptyset$
\For{each $v \in \{v\in V / dg(v)\geq3\}$}
	\State $currentDepth \leftarrow GetFurthestDistance(v, minDepth)$
    \If{$currentDepth = minDepth$}
    	\State $P \leftarrow P \cup \{v\}$
    \ElsIf{$currentDepth < minDepth$}
    	\State $P \leftarrow \{v\}$
    \EndIf
\EndFor
\State $P \leftarrow \{v \in P / dg(v) \geq dg(v') \forall v' \in P\}$

\State \Return $\arg\min_{v} \{ID(v) / v \in P\}$ 
\end{algorithmic}
\end{algorithm}

\section{Coordination des agents}
\paragraph{}Partant du constat qu'il arrive que deux agents explorateurs explorent la même zone de l'environnement, nous avons décidé d'implémenter une stratégie simple de coordination. Nous souhaitions utiliser les informations déjà à notre disposition pour la réaliser, et ne pas introduire de nouveau protocole (toujours dans notre objectif de limiter le nombre de messages échangés).
\paragraph{}Lors de l'échange des graphes entre deux agents, les agents échangent non seulement leurs connaissances sur l'environnement, mais aussi les positions connues des autres agents. Ainsi, une fois l'échange effectué, la dernière position des agents sont connues des deux agents. C'est cette information que nous utilisons pour coordonner un minimum les agents entre eux.
\paragraph{}L'algorithme repose sur le type des agents. Dans un premier temps nous avons considéré les agents explorateurs, puis nous avons étendu ce dernier aux agents collecteurs de même type de trésor. Le principe de l'algorithme est le suivant :
\begin{itemize}
	\item l'agent calcule ses cibles normalement
    \item l'agent utilise l'algorithme de Dijkstra depuis chaque cible vers les positions supposées des autres agents de même type et lui-même. Si le chemin le plus court mène à l'agent lui-même, la cible est valable. Sinon, un autre agent est plus près et la cible est abandonnée.
\end{itemize}
En suivant cette stratégie, on construit ainsi une frontière au delà de laquelle l'agent ne se rendra pas.

\paragraph{}L'utilisation de cette méthode soulève un problème : lorsque les représentations mentales de deux agents ne correspondent pas, il est possible que les deux agents attribuent le même nœud l'un à l'autre. Aussi, ce dernier ne sera visité pas aucun des deux agents. Pour pallier à ce problème, nous avons décidé d'effacer périodiquement les positions supposées des autres agents. Ainsi, d'une part cela permet aux agents de changer éventuellement de secteur, et d'autre part, il est beaucoup moins probable que le problème d'alignement entraîne la présence de nœuds non visités.

\paragraph{Avantages}l'avantage principal de cette approche est d'apporter une coordination sommaire tout en n'augmentant pas le nombre de messages échangés. La répartition des agents est plus harmonieuse sur l'ensemble de la carte, ce qui accélère la vitesse de découverte de cette dernière, mais aussi la vitesse de collecte des différents trésors.

\paragraph{Inconvénients}L'inconvénient de cette méthode est principalement au niveau du calcul des cibles potentielles. Comme il faut trier les cibles et éliminer celles couvertes par une autre agent, on ajoute une couche de calculs supplémentaires. Néanmoins, si on compare ce surcoût avec celui d'un protocole de communication classique, cet argument n'est pas vraiment valable.

\section{Conclusion}
\paragraph{}Pour conclure, nous avons développé trois types d'agents pour explorer l'environnement, collecter et stocker les trésors. Afin de partager les informations sur l'environnement, nous avons mis en place un protocole d'échange de graphe. Les graphes sont la représentation interne à chaque agent de l'environnement. Ces derniers permettent de positionner les différents trésors, leur valeur, la position supposée des autres agents ... Lorsque plusieurs agents évoluent dans un environnement partagé, il peut y avoir des situations où les agents se retrouvent inter-bloqués. Pour résoudre ces situations, nous avons mis en place un protocole de résolution des inter-blocages qui tient compte des agents muets. Enfin, pour une exploration et une collecte efficace, nous avons mis en place une coordination basée sur les connaissances des agents.
\end{document}
