package graph;

import java.io.Serializable;

/**
 * An utility class designed to hold two values.
 * @param <X> First value type
 * @param <Y> Second value type
 */
public class Couple<X, Y> implements Serializable {
    private X x;
    private Y y;

    /**
     * Create a new couple an provide both values.
     * @param x first value
     * @param y second value
     */
    public Couple(X x, Y y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Get first value
     * @return a value with type X
     */
    public X getX() {
        return x;
    }

    /**
     * Set the second value.
     * @param x a value with type X
     */
    public void setX(X x) {
        this.x = x;
    }

    /**
     * Get second value
     * @return a value with type Y
     */
    public Y getY() {
        return y;
    }

    /**
     * Set second value.
     * @param y a value with type Y
     */
    public void setY(Y y) {
        this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (o != null && o.getClass().equals(getClass())) {
            Couple<?, ?> c = (Couple<?, ?>) o;
            return c.getX().equals(getX()) && c.getY().equals(getY());
        }
        return false;
    }
}
