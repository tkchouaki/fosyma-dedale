package graph;

import java.util.Comparator;

/**
 * A comparator designed to compare Couple<X, Integer>.
 * @param <X> the other value
 */
public class DistCoupleComparator<X> implements Comparator<Couple<X, Integer>> {

    @Override
    public int compare(Couple<X, Integer> o1, Couple<X, Integer> o2) {
        return Integer.compare(o1.getY(), o2.getY());
    }
}
