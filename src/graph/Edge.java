package graph;

import java.io.Serializable;

/**
 * Edges represents a link between to vertices. Links have no directions, so A --> B is the same as B --> A.
 */
public class Edge implements Serializable{

    // ATTRIBUTES
    private Vertex vertex1;
    private Vertex vertex2;


    // CONSTRUCTOR
    /**
     * Create a new Edge linking vertex1 and vertex2.
     * @param vertex1 one of the ends
     * @param vertex2 the other
     */
    public Edge(Vertex vertex1, Vertex vertex2)
    {
        this.vertex1 = vertex1;
        this.vertex2 = vertex2;
    }

    @Override
    public boolean equals(Object o)
    {
        if(o != null && o.getClass().equals(getClass()))
        {
            Edge oEdge = (Edge) o;
            Vertex v1 = oEdge.getVertex1();
            Vertex v2 = oEdge.getVertex2();
            return (v1.equals(this.vertex1) && v2.equals(this.vertex2))
                    || (v1.equals(this.vertex2) && v2.equals(this.vertex1));
        }
        return false;
    }

    /**
     * Get the first declared end
     * @return a Vertex
     */
    public Vertex getVertex1() {
        return vertex1;
    }

    /**
     * Get the second declared end
     * @return a Vertex
     */
    public Vertex getVertex2() {
        return vertex2;
    }

    /**
     * Check if the provided vertex is one of the Edge's ends.
     * @param v a Vertex
     * @return true if v is an end, else false
     */
    public boolean containsVertex(Vertex v)
    {
        return this.vertex1.equals(v) || this.vertex2.equals(v);
    }

    /**
     * Get the opposite end of v. If v is not an end, return null.
     * @param v a Vertex
     * @return a Vertex or null
     */
    public Vertex getNeighbour(Vertex v)
    {
        if (this.vertex1.equals(v)) return vertex2;
        if (this.vertex2.equals(v)) return vertex1;
        return null;
    }

    /**
     * This method computes the hash code of an edge which consists of the hash code of its vertices
     * @return
     * The hash code of the edge
     */
    @Override
    public int hashCode() {
        int hash1 = vertex1.hashCode();
        int hash2 = vertex2.hashCode();
        int temp;
        if(hash1 > hash2)
        {
            temp = hash1;
            hash1 = hash2;
            hash2 = temp;
        }
        String hash = hash1 + "-" + hash2;
        return hash.hashCode();
    }
}
