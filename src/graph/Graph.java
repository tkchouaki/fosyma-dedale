package graph;

import env.Attribute;
import mas.agents.AgentDescription;
import mas.agents.PirateAgent;
import org.graphstream.graph.IdAlreadyInUseException;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.ui.swingViewer.ViewPanel;
import org.graphstream.ui.view.Viewer;
import util.Util;

import javax.swing.*;
import java.awt.*;
import java.io.Serializable;
import java.util.*;
import java.util.List;


/**
 * This class represents the agent's knowledge on its environment
 * It contains the vertices and edges known by the agent and its position
 */
public class Graph implements Serializable {

    /**
     * This Map represents the Graph structure (edges and vertices)
     */
    private Map<Vertex, Set<Edge>> edges;

    /**
     * This is for the graphical representation of the information
     */
    transient private org.graphstream.graph.Graph displayedGraph;

    /**
     * This Set contains the discovered vertices by the agent (seen but not visited)
     */
    private Set<Vertex> discoveredVertices;

    /**
     * This Set contains the explored vertices by the agent (seen and visited)
     */
    private Set<Vertex> exploredVertices;

    /**
     * The current position of the agent
     */
    private Vertex currentPosition;

    /**
     * The last update date of the information
     */
    private Date lastUpdate;

    private AgentDescription myAgentDescription;

    private Vertex centralPoint;


    public Graph(String graphName, AgentDescription myAgentDescription)
    {
        this(new HashMap<>(), graphName, myAgentDescription);
    }

    /**
     * Create a new graph with given edges and graph name.
     *
     * @param edges     a set of edges between vertices
     * @param graphName a name
     */
    public Graph(Map<Vertex, Set<Edge>> edges, String graphName, AgentDescription myAgentDescription) {

        this.myAgentDescription = myAgentDescription;

        this.edges = edges;
        this.displayedGraph = new SingleGraph(graphName);
        Viewer viewer = displayedGraph.display();
        ViewPanel viewPanel = viewer.getDefaultView();
        Container container = viewPanel.getParent();
        JRootPane rootPane = viewPanel.getRootPane();
        JFrame frame = (JFrame) rootPane.getParent();
        frame.setTitle(graphName);


        String defaultNodeStyle = "node {" + "fill-color: black;" + " size-mode:fit;text-alignment:under; text-size:14;text-color:white;text-background-mode:rounded-box;text-background-color:black;}";
        String nodeStyle_discovered = "node.discovered {" + "fill-color: red;" + "}";
        String nodeStyle_agent = "node.agent {" + "fill-color: blue;" + "}";
        String nodeStyle_otherAgent = "node.otheragent {" + "fill-color: purple;" + "}";
        String nodeStyle_explored = "node.explored {" + "fill-color: green;" + "}";
        String nodeStyle_myTreasureType = "node.myTreasureType {" + "fill-color: yellow;" + "}";
        String nodeStyle_notMyTreasureType = "node.notMyTreasureType {" + "fill-color: grey;" + "}";
        String nodeStyle_golem = "node.golem {" + "fill-color: orange;" + "}";
        String nodeStyle = defaultNodeStyle + nodeStyle_discovered + nodeStyle_agent + nodeStyle_otherAgent + nodeStyle_explored + nodeStyle_myTreasureType + nodeStyle_notMyTreasureType + nodeStyle_golem;

        displayedGraph.setAttribute("ui.stylesheet", nodeStyle);

        this.discoveredVertices = new HashSet<>();
        this.exploredVertices = new HashSet<>();
        this.currentPosition = null;
        this.lastUpdate = new Date();

    }

    /**
     * Add a new Vertex. Ignore if present
     * @param v the vertex
     * @return
     * true if added, false if ignored
     */
    public boolean addVertex(Vertex v)
    {
        if (this.edges.containsKey(v))
        {
            //if the vertex already exists, it is just updated
            Vertex myVertex = this.getVertexById(v.getId());
            return myVertex.updateFromVertex(v);
        }
        else
        {
            //otherwise, it is added
            this.edges.put(v, new HashSet<>());
            this.setLastUpdate();
            try
            {
                Node node = this.displayedGraph.addNode(v.getId());
                node.addAttribute("ui.label", v.getId());
            }
            catch (IdAlreadyInUseException e){
                return true;
            }
            return true;
        }
    }


    /**
     * This methods retrieves a vertex by its Id
     * @param id
     * The id of the wanted vertex
     * @return
     * The vertex with the given Id, Null if it doesn't exist
     */
    private Vertex getVertexById(String id)
    {
        for(Vertex v : edges.keySet())
        {
            if(v.getId().equals(id))
            {
                return v;
            }
        }
        return null;
    }

    /**
     * Add a new Vertex (provided as a string). Ignore if present
     *
     * @param id the vertex id
     * @return true if added, false if ignored
     */
    public boolean addVertex(String id) {
        return this.addVertex(new Vertex(id));
    }


    /**
     * Check if vertex v is in the graph.
     *
     * @param v the vertex
     * @return true or false
     */
    public boolean containsVertex(Vertex v) {
        return this.edges.containsKey(v);
    }

    /**
     * Check if vertex idV (provided as string) is in the graph.
     *
     * @param idV the vertex
     * @return true or false
     */
    public boolean containsVertex(String idV) {
        return this.containsVertex(new Vertex(idV));
    }

    /**
     * Add an edge to the graph. If declared vertices are not in the graph, they will be added.
     *
     * @param e the new edge
     * @return true if added, false if ignored
     */
    public boolean addEdge(Edge e) {
        if (this.containsEdge(e)) return false;
        this.addVertex(e.getVertex1());
        this.addVertex(e.getVertex2());
        this.edges.get(e.getVertex1()).add(e);
        this.edges.get(e.getVertex2()).add(e);
        if (displayedGraph.getEdge(e.getVertex1().getId() + e.getVertex2().getId()) == null && displayedGraph.getEdge(e.getVertex2().getId() + e.getVertex1().getId()) == null)
            displayedGraph.addEdge(e.getVertex1().getId() + e.getVertex2().getId(), e.getVertex1().getId(), e.getVertex2().getId());
        this.setLastUpdate();
        return true;
    }

    /**
     * Add a new edge between two vertices (provided as Vertex). Creates them if necessary.
     *
     * @param v1 first vertex
     * @param v2 second vertex
     * @return true if added, false if ignored
     */
    public boolean addEdge(Vertex v1, Vertex v2) {
        return this.addEdge(new Edge(v1, v2));
    }

    /**
     * Add a new edge between two vertices (provided as String). Creates them if necessary.
     *
     * @param idV1 first vertex
     * @param idV2 second vertex
     * @return true if added, false if ignored
     */
    public boolean addEdge(String idV1, String idV2) {
        return this.addEdge(new Vertex(idV1), new Vertex(idV2));
    }

    /**
     * This method checks if an edge exists in the Graph
     *
     * @param edge The edge that we want to check its existance in the Graph
     * @return A boolean indicating if the edge exists in the Graph
     */
    public boolean containsEdge(Edge edge) {
        return this.getEdges().contains(edge);
    }

    /**
     * Get all vertices declared in the graph
     *
     * @return a set of vertices
     */
    public Set<Vertex> getVertices() {
        return new HashSet<>(this.edges.keySet());
    }

    /**
     * Get all declared edges in the graph.
     *
     * @return a set of edges.
     */
    public Set<Edge> getEdges() {
        Set<Edge> result = new HashSet<>();
        Collection<Set<Edge>> collection = this.edges.values();
        for (Set<Edge> set : collection) {
            result.addAll(set);
        }
        return result;
    }

    /**
     * Run Dijkstra algorithm to find a path to the closest Vertex in possibleEndings, starting at vertex start. If no
     * path is found, returns an empty list. If several vertices are at the same distance, pick a random one.
     *
     * @param start           the start vertex
     * @param possibleEndings a set of vertices you want to reach.
     * @return a list of vertices representing the path from start to one end vertex.
     */
    public List<Vertex> getPath(Vertex start, Set<Vertex> possibleEndings)
    {
        return getPath(start, possibleEndings, new HashSet<>());
    }

    /**
     * Run Dijkstra algorithm to find a path to the closest Vertex in possibleEndings, starting at vertex start. If no
     * path is found, returns an empty list. If several vertices are at the same distance, pick a random one.
     *
     * @param start           the start vertex
     * @param possibleEndings a set of vertices you want to reach.
     * @param forbiddenVertices a set of vertices you don't want your path to include
     * @return a list of vertices representing the path from start to one end vertex.
     */
    public List<Vertex> getPath(Vertex start, Set<Vertex> possibleEndings, Set<Vertex> forbiddenVertices) {
        // The path will be written in that list
        List<Vertex> result = new ArrayList<>();

        // Check if at least one end is a vertex in the graph (and same for start)
        List<Vertex> myPossibleEndings = new ArrayList<>();
        if (!this.containsVertex(start)) return result;
        for (Vertex ending : possibleEndings) {
            if (this.containsVertex(ending)) myPossibleEndings.add(ending);
        }
        if (myPossibleEndings.size() == 0) return result;

        // Map Vertex -> Parent
        Map<Vertex, Vertex> parents = new HashMap<>();
        parents.put(start, null);

        // A Map Vertex -> Couple<Vertex, Integer> to kep track ok objects in the priority queue
        Map<Vertex, Couple<Vertex, Integer>> track = new HashMap<>();

        // Priority queue containing opened vertices
        PriorityQueue<Couple<Vertex, Integer>> openOnes = new PriorityQueue<>(new DistCoupleComparator<>());

        // Add the start vertex
        Couple<Vertex, Integer> startCouple = new Couple<>(start, 0);
        openOnes.add(startCouple);
        track.put(start, startCouple);

        // Closed vertices
        Set<Vertex> closedOnes = new HashSet<>();

        Vertex currentParent = null;
        while (openOnes.size() > 0 && !myPossibleEndings.contains(currentParent)) {
            // Get the closest
            Couple<Vertex, Integer> currentCouple = openOnes.poll();
            Integer min = currentCouple.getY();

            // Add some random if many vertices are at the same distance
            List<Couple<Vertex, Integer>> possibleNextCurrent = new ArrayList<>();
            possibleNextCurrent.add(currentCouple);
            while (openOnes.size() > 0 && openOnes.peek().getY().intValue() == min) {
                possibleNextCurrent.add(openOnes.poll());
            }
            currentCouple = possibleNextCurrent.remove(new Random().nextInt(possibleNextCurrent.size()));
            currentParent = currentCouple.getX();

            // Add remaining vertices
            openOnes.addAll(possibleNextCurrent);

            // Get all near vertices and remove explored ones (and forbidden)
            Set<Vertex> neighbours = this.getNeighbours(currentParent);
            neighbours.removeAll(closedOnes);
            neighbours.removeAll(forbiddenVertices);

            for (Vertex neighbour : neighbours) {
                if (!track.containsKey(neighbour)) {
                    // Build the new couple
                    Couple<Vertex, Integer> neighbourCouple = new Couple<>(neighbour, min + 1);

                    // Add it to openONes
                    openOnes.add(neighbourCouple);
                    track.put(neighbour, neighbourCouple);

                    // Update parent
                    parents.put(neighbour, currentParent);
                } else {
                    if (track.get(neighbour).getY() > min + 1) {
                        // We have to remove the previous couple
                        openOnes.remove(track.get(neighbour));

                        // Build the new one
                        Couple<Vertex, Integer> neighbourCouple = new Couple<>(neighbour, min + 1);

                        // Add it to openOnes
                        openOnes.add(neighbourCouple);
                        track.put(neighbour, neighbourCouple);

                        // Update parent
                        parents.put(neighbour, currentParent);
                    }
                }
            }
            // The current vertex is now explored, let's put it in the closed set
            closedOnes.add(currentParent);
        }

        // Have we reached an end vertex ?
        if (myPossibleEndings.contains(currentParent)) {
            // Build the path
            Vertex current = currentParent;
            result.add(current);
            do {
                current = parents.get(current);
                if (current != null) result.add(0, current);
            } while (current != null);

        }
        ArrayList<Vertex> resultWithMyVertices = new ArrayList<>();
        for(Vertex v : result)
        {
            resultWithMyVertices.add(this.getVertexById(v.getId()));
        }
        return resultWithMyVertices;
    }

    /**
     * Get all vertices linked with an edge to vertex v.
     * @param v the vertex
     * @return a set of vertices
     */
    public Set<Vertex> getNeighbours(Vertex v) {
        Set<Edge> edges = this.edges.get(v);
        Set<Vertex> result = new HashSet<>();
        for (Edge edge : edges) {
            result.add(edge.getNeighbour(v));
        }
        return result;
    }

    /**
     * Change the ui.class attribute for vertex v. (color)
     *
     * @param v         the vertex
     * @param className the new class name
     */
    public void setVertexClass(Vertex v, String className) {
        this.displayedGraph.getNode(v.getId()).setAttribute("ui.class", className);
    }

    /**
     * For each vertex in vertices, put the ui.class to className. (vertices color)
     *
     * @param vertices  the vertices
     * @param className the class name
     */
    public void setVerticesClass(Collection<Vertex> vertices, String className) {
        for (Vertex vertex : vertices) {
            setVertexClass(vertex, className);
        }
    }


    /**
     * This method adds all the information of another Graph object to the current Object
     *
     * @param graph The Graph object whose vertices and edges will be added to the graph
     * @return A boolean indicating if the current object was modified or not
     */
    public boolean addInfo(Graph graph) {
        boolean modified = false;
        //add the vertices of the given graph
        for (Vertex vertex : graph.getVertices()) {
            if (this.addVertex(vertex)) {
                modified = true;
            }
        }
        //add the edges of the given graph
        for (Edge edge : graph.getEdges()) {
            if (this.addEdge(edge)) {
                modified = true;
            }
        }
        //the explored vertices of the given graph become explored in the current graph
        for(Vertex v : graph.exploredVertices)
        {
            if(this.discoveredVertices.contains(v))
            {
                this.discoveredVertices.remove(v);
            }
            if(!this.exploredVertices.contains(v))
            {
                modified = true;
                this.exploredVertices.add(v);
            }
        }
        //the discovered vertices of the given graph that are not explored in the current graph become discovered in the current graph
        for(Vertex v : graph.discoveredVertices)
        {
            if(!this.exploredVertices.contains(v))
            {
                modified = true;
                this.discoveredVertices.add(v);
            }
        }
        this.removeAgentFromItsVertex(graph.myAgentDescription.getName());
        this.getVertexById(graph.getCurrentPosition().getId()).setPresentAgent(graph.myAgentDescription);
        //updating the styles to get an appropriate vie
        this.updateStyles();
        if(modified) setLastUpdate();
        return modified;
    }

    /**
     * This method gives the Set of discovered Vertices (seen but not visited), for coherence purposes the Set is unmodifiable
     * @return
     * A Set containing the discovered vertices
     */
    public Set<Vertex> getDiscoveredVertices() {
        return Collections.unmodifiableSet(this.discoveredVertices);
    }

    /**
     * This method gives the Set of explored Vertices (seen and visited), for coherence purposes the Set is unmodifiable
     * @return
     * A Set containing the explored vertices
     */
    public Set<Vertex> getExploredVertices() {
        return Collections.unmodifiableSet(this.exploredVertices);
    }

    /**
     * This method adds a vertex from the graph to the Set of explored vertices
     * @param vertex
     * The vertex to add to the Set of explored vertices, the vertex has to be already in the graph
     * @return
     * true if the vertex was added to the Set of explored vertices, false if it doesn't exist in the graph
     */
    public boolean addExploredVertex(Vertex vertex)
    {
        vertex = this.getVertexById(vertex.getId());
        if(vertex == null)
        {
            return false;
        }
        this.exploredVertices.add(vertex);
        return true;
    }

    /**
     * This method adds a vertex from the graph to the Set of discovered vertices
     * @param vertex
     * The vertex to add to the Set of discovered vertices, the vertex has to be already in the graph
     * @return
     * true if the vertex was added to the set of discovered vertices, false if it doesn't exist in the graph
     */
    public boolean addDiscoveredVertex(Vertex vertex)
    {
        vertex = this.getVertexById(vertex.getId());
        if(vertex == null)
        {
            return false;
        }
        this.discoveredVertices.add(vertex);
        return true;
    }

    /**
     * This method checks if a given vertex is in the Set of explored vertices
     * @param vertex
     * The vertex whose belonging to the set of explored vertices has to be checked
     * @return
     * true if the given vertex belongs to the set of explored vertices, false otherwise
     */
    public boolean isVertexExplored(Vertex vertex)
    {
        return this.exploredVertices.contains(vertex);
    }

    /**
     * This method removes a given vertex from the Set of discovered vertices
     * @param vertex
     * The vertex we want to remove from the Set of discovered vertices
     * @return
     * True if the vertex was in the Set of discovered vertices (it was removed in this case)
     * False otherwise
     */
    public boolean removeDiscoveredVertex(Vertex vertex)
    {
        return this.discoveredVertices.remove(vertex);
    }

    /**
     * This method sets the currentPosition attribute to a given Vertex
     * if the given Vertex is not contained in the graph, it is added
     * @param vertex
     * The vertex to set the currentPosition attribute to
     */
    public void setCurrentPosition(Vertex vertex)
    {
        if(!this.containsVertex(vertex))
        {
            //if the vertex is not contained in the graph, it is added
            this.addVertex(vertex);
        }
        //getting the name of the agent present in the vertex, which is supposedly the name of the agent possessing the current graph
        this.removeAgentFromItsVertex(this.myAgentDescription.getName());
        //we set the current agent as present in given vertex
        vertex = this.getVertexById(vertex.getId());
        vertex.setPresentAgent(this.myAgentDescription);
        //we the the currentPosition attribute to the given vertex
        this.currentPosition = vertex;
    }

    /**
     * This method updates the styles of the vertices in the GraphStream Graph object
     * To get a proper graphical representation
     */
    public void updateStyles()
    {
        this.setVerticesClass(this.exploredVertices, "explored");
        this.setVerticesClass(this.discoveredVertices, "discovered");
        if(this.currentPosition!=null)
        {
            this.setVertexClass(this.currentPosition, "agent");
        }
        for(Vertex v : this.getVertices())
        {
            if(v.getTreasureQuantity()>0)
            {
                if(v.getTreasureType().equals(this.myAgentDescription.getTreasureType()))
                {
                    this.setVertexClass(v, "myTreasureType");
                }
                else
                {
                    this.setVertexClass(v, "notMyTreasureType");
                }
            }
            if(v.getPresentAgentDescription() != null && !v.getPresentAgentDescription().getName().equals(this.myAgentDescription.getName()))
            {
                this.setVertexClass(v, "otheragent");
            }
            if(v.isGolemStench())
            {
                this.setVertexClass(v, "golem");
            }
        }
    }


    /**
     * This method retrives the currentPosition attribute
     * @return
     * The vertex representing the current position of the current agent
     */
    public Vertex getCurrentPosition() {
        return currentPosition;
    }

    /**
     * This method sets lastUpdate attribute to current date
     */
    private void setLastUpdate()
    {
        this.lastUpdate = new Date();
    }

    /**
     * This methods retrieves the lastUpdate Date of the graph
     * @return
     * The lastUpdate Date of the graph
     */
    public Date getLastUpdate() {
        return lastUpdate;
    }

    /**
     * This method retrieves the vertices with some treasure.
     * @return vertices with a treasure
     */
    public Set<Vertex> getWithTreasureVertices() {
        Set<Vertex> vertices = new HashSet<>();

        // Computes nodes with treasure
        for (Vertex v : this.getVertices()) {
            if (v.getTreasureType() != null && v.getTreasureType().equals(Attribute.TREASURE.getName()) && v.getTreasureQuantity()>0) {
                vertices.add(v);
            }
        }
        return vertices;
    }

    /**
     * This method returns the vertices with some diamonds inside it.
     * @return a Set of vertices with diamonds
     */
    public Set<Vertex> getWithDiamondsVertices() {
        Set<Vertex> vertices = new HashSet<>();

        // Computes nodes with diamonds
        for (Vertex v : getExploredVertices()) {
            if (v.getTreasureType() != null && v.getTreasureType().equals(Attribute.DIAMONDS.getName()) && v.getTreasureQuantity()>0) {
                vertices.add(v);
            }
        }
        for (Vertex v : getDiscoveredVertices()) {
            if (v.getTreasureType() != null && v.getTreasureType().equals(Attribute.DIAMONDS.getName())) {
                vertices.add(v);
            }
        }

        return vertices;
    }

    /**
     * This method computes the central point of the graph
     * It is the vertex that minimizes the maximum distance to other vertices
     * if multiple central points are found, we consider the one with the maximum degree
     * if there is still more than one possibility, we consider the first vertex from the alphabetical order of its ID.
     * the central point is computed only among the vertices that have a degree of at least 3
     * @return
     * the central point of the graph
     */
    public Vertex getCentralPoint()
    {
        if(this.centralPoint != null)
        {
            return this.centralPoint;
        }
        List<Vertex> possibleResults = new ArrayList<>();
        Vertex result=null;
        int minDepth=-2;
        int maxDegree=-1;
        int currentDepth;
        int currentDegree;

        for(Vertex vertex : this.getVertices())
        {
            //we consider only the vertices that have a degree of at least 3
            if(this.edges.get(vertex).size()>2)
            {
                //we compute the maximum distance rom current vertex
                currentDepth = getMaxDistanceFromVertex(vertex, minDepth+1);

                //if we're on the first iteration or we have found a similar or better depth
                if(minDepth<0 || currentDepth<=minDepth)
                {
                    //if the new depth is strictly better, we clear the list of possible results and we update the minDepth
                    if(currentDepth<minDepth || minDepth<0)
                    {
                        possibleResults.clear();
                        minDepth = currentDepth;
                    }
                    //we add the vertex to the list of possible results
                    possibleResults.add(vertex);
                }
            }
        }

        //now among the possible results, we consider the one with the maximum degree
        for(Vertex vertex : possibleResults)
        {
            currentDegree = this.edges.get(vertex).size();
            if(maxDegree<0 || currentDegree >= maxDegree)
            {
                if(currentDegree==maxDegree)
                {
                    //in case of multiple vertices with the same maximum degree, we consider the first from the alphabetical point of view of the ID
                    if(result==null || result.getId().compareTo(vertex.getId()) < 0)
                    {
                        result = vertex;
                    }
                }
                else
                {
                    maxDegree = currentDegree;
                    result = vertex;
                }
            }
        }
        if(this.discoveredVertices.size() == 0)
        {
            this.centralPoint = result;
        }
        return result;
    }

    /**
     * This method computes the distance from a given vertex to the furthest vertex accessible from it and returns it if it does not exceeds a maximum value
     * If so, this maximum value is returned
     * @param v
     * A vertex
     * @param max
     * The maximum value that the return value will not exceed
     * @return
     * The distance from the given vertex to the furthest vertex accessible from it
     */
    public int getMaxDistanceFromVertex(Vertex v, int max)
    {
        //we just comput the accessibility tree and return its depth
        List<Set<Vertex>> accessibilityTree = getAccessibilityTreeFromVertex(v, max);
        return accessibilityTree.size();
    }

    /**
     * This method builds the accessibility tree from a given vertex
     * The root of the tree will be the given vertex
     * The depth of the tree will not exceed a given integer
     * @param root
     * The vertex that will be the root of the tree
     * @param maxDepth
     * The maximum depth that the tree can have
     * @return
     * A list of sets representing the tree, each element of the set corresponding to an accessibility level
     */
    public List<Set<Vertex>> getAccessibilityTreeFromVertex(Vertex root, int maxDepth)
    {
        //some useful variables
        List<Set<Vertex>> levels = new ArrayList<>();
        Set<Vertex> newLevel;
        Set<Vertex> oldLevel;
        //this one contains all the encountered vertices (basically the union of all levels)
        Set<Vertex> seen = new HashSet<>();
        //if the given vertex is not even in the graph, nothing
        if(!this.containsVertex(root))
        {
            return levels;
        }
        newLevel = new HashSet<>();
        newLevel.add(root);
        seen.add(root);
        while(newLevel.size()!=0 && (maxDepth<0 || maxDepth>levels.size()))
        {
            levels.add(newLevel);
            if(levels.size() >= maxDepth && maxDepth > 0)
            {
                break;
            }
            oldLevel = newLevel;
            newLevel = new HashSet<>();
            //adding the unseen neighbours of the vertices of the old level to a new level
            for(Vertex vertex: oldLevel)
            {
                for(Edge edge: this.edges.get(vertex))
                {
                    Vertex newVertex = edge.getNeighbour(vertex);
                    if(!seen.contains(newVertex))
                    {
                        newLevel.add(newVertex);
                        seen.add(newVertex);
                    }
                }
            }
        }
        return levels;
    }

    /**
     * This method returns the degree of a given vertex if it is contained in the graph, -1 otherwise
     * @param v
     * A vertex
     * @return
     * the degree of the given vertex if it contained in the graph, -1 otherwise
     */
    public int getVertexDegree(Vertex v) {
        if (containsVertex(v)) {
            return this.edges.get(v).size();
        }
        return -1;
    }

    public List<Vertex> findMatchingVertex(int minDegree, Collection<Vertex> forbiddenVertices) {
        List<Vertex> matchingVertices = new ArrayList<>();
        Set<Vertex> closedVertices = new HashSet<>();

        Queue<Vertex> openedVertices = new ArrayDeque<>();
        openedVertices.add(this.getCurrentPosition());

        while (openedVertices.size() > 0) {
            Vertex root = openedVertices.poll();
            for (Vertex v : this.getNeighbours(root)) {
                if (!closedVertices.contains(v) && !forbiddenVertices.contains(v)) {
                    openedVertices.add(v);
                    if (getVertexDegree(v) >= minDegree) {
                        matchingVertices.add(v);
                    }
                }
            }
            closedVertices.add(root);
        }
        return matchingVertices;
    }

    public Set<Vertex> getVerticesWithMinDegree(int minDegree)
    {
        Set<Vertex> result = new HashSet<>();
        for(Vertex v : getVertices())
        {
            int degree = this.getVertexDegree(v);
            if(degree >= minDegree)
            {
                result.add(v);
            }
        }
        return result;
    }

    /**
     * This method computes paths for two agents each to one if its targets in a way that solves interlocks.
     * There is a primary agent and a secondary agent. This method will try first to satisfy the primary agent.
     * If obliged to, the secondary agent can get a path which would not lead him to any of its targets but clears the way for the primary agent
     * @param targets
     * A HashMap whose keys are the names of the agents and the values are couples containing the current position and the target of each agent
     * @param primaryAgent
     * The name of the primary agent
     * @return
     * A HashMap whose keys are the names of the agents and the values are lists of vertices representing the agent's paths
     */
    public HashMap<String, List<Vertex>> getPathsForTwoInterlockedAgents(HashMap<String, Couple<Vertex, Set<Vertex>>> targets, String primaryAgent)
    {
        Set<String> keys = targets.keySet();

        //some verifications

        //verifying that the map contains two keys including the primary agent
        if(keys.size() != 2 || !keys.contains(primaryAgent)) return null;

        String secondaryAgent=primaryAgent;

        //getting the name of the secondary agent
        for(String key : keys)
        {
            if(!key.equals(primaryAgent))
            {
                secondaryAgent = key;
                break;
            }
        }

        //verifying that the primary agent is different from the secondary agent
        if(secondaryAgent.equals(primaryAgent)) return null;


        Vertex primaryAgentPosition = targets.get(primaryAgent).getX();
        Vertex secondaryAgentPosition = targets.get(secondaryAgent).getX();

        Set<Vertex> primaryAgentTargets = targets.get(primaryAgent).getY();
        Set<Vertex> secondaryAgentTargets = targets.get(secondaryAgent).getY();

        List<Vertex> primaryAgentPath = new ArrayList<>();
        List<Vertex> secondaryAgentPath = new ArrayList<>();

        //useful variables
        Set<Vertex> forbidden;
        Set<Vertex> tempTargets;


        if(primaryAgentTargets.size()>0 || secondaryAgentTargets.size() >0)
        {
            //if one of the agents has no targets, he has to move to let the other one pass
            if(primaryAgentTargets.size() ==0 || secondaryAgentTargets.size() ==0)
            {
                Set<Vertex> agentsTargets;
                Vertex agentWithTargetsPosition;
                Vertex agentWithoutTargetsPosition;
                List<Vertex> agentWithTargetsPath=new ArrayList<>();
                List<Vertex> agentWithoutTargetsPath=new ArrayList<>();

                //determining which of the agents has no targets
                if(primaryAgentTargets.size() == 0)
                {
                    agentWithoutTargetsPosition = primaryAgentPosition;
                    agentWithTargetsPosition = secondaryAgentPosition;
                    agentsTargets = secondaryAgentTargets;
                }
                else
                {
                    agentWithoutTargetsPosition = secondaryAgentPosition;
                    agentWithTargetsPosition = primaryAgentPosition;
                    agentsTargets = primaryAgentTargets;
                }

                //for every target, we try to determine a way that could let the agent having it reach it by moving the agent that has no target to a point that would not bother
                for(Vertex target : agentsTargets)
                {
                    tempTargets = new HashSet<>();
                    tempTargets.add(target);
                    //compute the path for this target
                    agentWithTargetsPath = this.getPath(agentWithTargetsPosition, tempTargets);
                    if(agentWithTargetsPath.size()>0)
                    {
                        //we try to determine a path for the other agent to a vertex that would not be in the way in the first agent's path
                        //the first agent's position is forbidden to pass by
                        forbidden = new HashSet<>();
                        forbidden.add(agentWithTargetsPosition);
                        //we allow him to go anywhere expect the vertices that are in the way of the first agent
                        tempTargets = this.getVertices();
                        tempTargets.removeAll(agentWithTargetsPath);
                        agentWithoutTargetsPath = this.getPath(agentWithoutTargetsPosition, tempTargets, forbidden);
                        if(agentWithoutTargetsPath.size()>0)
                        {
                            break;
                        }
                    }
                }

                if(agentWithoutTargetsPath.size() ==0 || agentWithoutTargetsPath.size() == 0 )
                {
                    tempTargets = this.getVerticesWithMinDegree(3);
                    Vertex vertexForAgentWithTargets = null;
                    Vertex selectedFork = null;
                    if(tempTargets.size() >0)
                    {
                        forbidden = new HashSet<>();
                        forbidden.add(agentWithoutTargetsPosition);
                        if(tempTargets.contains(agentWithTargetsPosition))
                        {
                            selectedFork = agentWithTargetsPosition;
                            agentWithTargetsPath = new ArrayList<>();
                        }
                        else
                        {
                            agentWithTargetsPath = this.getPath(agentWithTargetsPosition, tempTargets, forbidden);
                            if(agentWithTargetsPath.size()>0)
                            {
                                selectedFork=agentWithTargetsPath.get(agentWithTargetsPath.size()-1);
                            }
                        }
                        if(selectedFork != null)
                        {
                            Set<Vertex> neighbours = this.getNeighbours(selectedFork);
                            if(agentWithoutTargetsPath.size()>0)
                            {
                                neighbours.removeAll(agentWithTargetsPath);
                            }
                            else
                            {
                                tempTargets = new HashSet<>();
                                tempTargets.add(agentWithoutTargetsPosition);
                                neighbours.removeAll(this.getPath(selectedFork, tempTargets));
                            }

                            vertexForAgentWithTargets = (Vertex) Util.randomSetPick(neighbours);

                            agentWithTargetsPath.addAll(this.getPath(vertexForAgentWithTargets, agentsTargets));
                            neighbours.remove(vertexForAgentWithTargets);
                            Vertex vertexForAgentWithoutTargets = null;

                            vertexForAgentWithoutTargets = (Vertex) Util.randomSetPick(neighbours);

                            tempTargets = new HashSet<>();
                            tempTargets.add(vertexForAgentWithoutTargets);
                            agentWithoutTargetsPath = this.getPath(agentWithoutTargetsPosition, tempTargets);
                        }
                    }
                }

                //if we find a path for each agent, we give each path to its agent
                if(agentWithTargetsPath.size()>0 && agentWithoutTargetsPath.size()>0)
                {
                    if(primaryAgentTargets.size() == 0)
                    {
                        primaryAgentPath = agentWithoutTargetsPath;
                        secondaryAgentPath = agentWithTargetsPath;
                    }
                    else
                    {
                        primaryAgentPath = agentWithTargetsPath;
                        secondaryAgentPath = agentWithoutTargetsPath;
                    }
                }
            }
            else
            {
                //in this case, both the agents have at least one target
                //for each target of the primary agent
                //we try to compute a path for the primary agent to the current target
                //then we try to compute a path for the secondary agent to one of its targets
                for(Vertex target : primaryAgentTargets)
                {
                    tempTargets = new HashSet<>();
                    tempTargets.add(target);
                    primaryAgentPath = this.getPath(primaryAgentPosition, tempTargets);
                    if(primaryAgentPath.size()>0)
                    {
                        //the secondary agent must not pass by the primary agent's initial position
                        forbidden = new HashSet<>();
                        forbidden.add(primaryAgentPosition);
                        //it also mustn't stop on the way of the primary agent
                        tempTargets = new HashSet<>(secondaryAgentTargets);
                        tempTargets.removeAll(primaryAgentPath);
                        secondaryAgentPath = this.getPath(secondaryAgentPosition, tempTargets, forbidden);
                        if(secondaryAgentPath.size()>0)
                        {
                            break;
                        }
                    }
                }
                if(secondaryAgentPath.size()==0)
                {
                    //if we cannot move both agent's to one of their respective targets
                    //we try to move the primary agent to one of its target while the secondary one just clears the way
                    HashMap<String, Couple<Vertex, Set<Vertex>>> newTargets = (HashMap<String, Couple<Vertex, Set<Vertex>>>) targets.clone();
                    newTargets.put(secondaryAgent, new Couple<>(secondaryAgentPosition, new HashSet<>()));
                    return getPathsForTwoInterlockedAgents(newTargets, primaryAgent);
                }
            }
        }
        //if we could compute a path for each agent, we just return them
        if(primaryAgentPath.size()>0 && secondaryAgentPath.size()>0)
        {
            HashMap<String, List<Vertex>> result = new HashMap<>();
            result.put(primaryAgent, primaryAgentPath);
            result.put(secondaryAgent, secondaryAgentPath);
            return result;
        }
        //otherwise, null is returned
        return null;
    }

    /**
     * This method returns the Vertex that hasn't been visited for the longest time
     * @return
     * A Vertex
     */
    public Vertex getOldestExploredVertex()
    {
        Set<Vertex> vertices = this.getVertices();
        Vertex result = null;
        Date min = new Date();
        for(Vertex vertex : vertices)
        {
            if(vertex.getLastVisited().getTime() < min.getTime())
            {
                result = vertex;
                min = vertex.getLastVisited();
            }
        }
        return result;
    }

    public int getTreasureQuantity()
    {
        return this.myAgentDescription.getTreasureQuantity();
    }


    public void emptyTreasure()
    {
        if(this.myAgentDescription.emptyTreasure())
        {
            this.setLastUpdate();
        }
    }

    public boolean addToTreasure(int picked)
    {
        if(this.myAgentDescription.addTreasure(picked))
        {
            this.setLastUpdate();
            return true;
        }
        return false;
    }

    public boolean removeAgentFromItsVertex(String agentName)
    {
        boolean result = false;
        for(Vertex v : this.getVertices())
        {
            if(agentName.equals(v.getPresentAgentName()))
            {
                v.setPresentAgent(null);
                result = true;
            }
        }
        return result;
    }

    public Set<Vertex> getVerticesWithAgents()
    {
        Set<Vertex> result = new HashSet<>();
        for(Vertex v : this.getVertices())
        {
            if(v.getPresentAgentDescription() != null)
            {
                result.add(v);
            }
        }
        return result;
    }



    public String getClosestAgentTo(Vertex v)
    {
        Set<Vertex> verticesWithAgents = this.getVerticesWithAgents();
        List<Vertex> path = this.getPath(v, verticesWithAgents);
        if(path != null && path.size()>0)
        {
            return this.getVertexById(path.get(path.size()-1).getId()).getPresentAgentDescription().getName();
        }
        else
        {
            return null;
        }
    }

    public String getClosestAgentTo(Vertex vertex, Class agentClass)
    {
        Set<Vertex> verticesWithAgents = this.getVerticesWithAgents();
        Set<Vertex> verticesWithGoodAgents = new HashSet<>();
        for(Vertex v : verticesWithAgents)
        {
            if(v.getPresentAgentClass() == agentClass)
            {
                verticesWithGoodAgents.add(v);
            }
        }
        List<Vertex> path = this.getPath(vertex, verticesWithGoodAgents);
        if(path != null && path.size()>0)
        {
            return path.get(path.size()-1).getPresentAgentDescription().getName();
        }
        else
        {
            return null;
        }
    }


    public String getClosestAgentTo(Vertex vertex, Class agentClass, String treasureType)
    {
        Set<Vertex> verticesWithAgents = this.getVerticesWithAgents();
        Set<Vertex> verticesWithGoodAgents = new HashSet<>();
        for(Vertex v : verticesWithAgents)
        {
            if(v.getPresentAgentClass() == agentClass && treasureType.equals(v.getPresentAgentTreasureType()))
            {
                verticesWithGoodAgents.add(v);
            }
        }
        List<Vertex> path = this.getPath(vertex, verticesWithGoodAgents);
        if(path != null && path.size()>0)
        {
            return path.get(path.size()-1).getPresentAgentDescription().getName();
        }
        else
        {
            return null;
        }
    }

    public Set<Vertex> getVerticesImClosestTo()
    {
        return this.getVerticesImClosestTo(this.getVertices());
    }

    public Set<Vertex> getVerticesImClosestTo(Set<Vertex> vertices)
    {
        Set<Vertex> result = new HashSet<>();
        for(Vertex v : vertices)
        {
            if(this.myAgentDescription.getName().equals(this.getClosestAgentTo(v)))
            {
                result.add(v);
            }
        }
        return result;
    }


    public Set<Vertex> getVerticesImClosestToAmongAgentsWithSameClass(Set<Vertex> vertices)
    {
        Set<Vertex> result = new HashSet<>();
        for(Vertex v : vertices)
        {
            if(this.myAgentDescription.getName().equals(this.getClosestAgentTo(v, this.myAgentDescription.getAgentClass())))
            {
                result.add(v);
            }
        }
        return result;
    }

    public Set<Vertex> getVerticesImClosestToAmongAgentsWithSameClassAndTreasureType(Set<Vertex> vertices)
    {
        Set<Vertex> result = new HashSet<>();
        for(Vertex v : vertices)
        {
            if(this.myAgentDescription.getName().equals(this.getClosestAgentTo(v, this.myAgentDescription.getAgentClass(), this.myAgentDescription.getTreasureType())))
            {
                result.add(v);
            }
        }
        return result;
    }
}
