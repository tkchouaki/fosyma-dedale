package graph;

import mas.agents.AgentDescription;

import javax.naming.ldap.HasControls;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * A simple wrapper around a String. Represents a vertex in a graph.
 */
public class Vertex implements Serializable {
    // ATTRIBUTES
    private String id;
    private Date lastSeen;
    private Date lastVisited;
    private String treasureType;
    private float treasureQuantity;
    private AgentDescription presentAgent;
    private boolean golemStench;

    /**
     * Returns the date corresponding to the last time the current vertex was seen
     * @return
     * A date
     */
    public Date getLastSeen() {
        return lastSeen;
    }

    /**
     * A setter for the lastSeen attribute
     * @param lastSeen
     * the new lastSeen value
     */
    public void setLastSeen(Date lastSeen) {
        this.lastSeen = lastSeen;
    }

    /**
     * Returns the date corresponding to the last time the current vertex was visited
     * @return
     * A Date
     */
    public Date getLastVisited() {
        return lastVisited;
    }

    /**
     * A setter for the lastVisited attribute
     * This setter verifies that the lastSeen Date is still more recent than the lastVisited date
     * otherwise, the lastSeen date is also set to the given value
     * @param lastVisited
     * the new lastVisited value
     */
    public void setLastVisited(Date lastVisited) {
        this.lastVisited = lastVisited;
        if(this.lastVisited.getTime() > this.lastSeen.getTime())
        {
            this.lastSeen = this.lastVisited;
        }
    }

    /**
     * This method returns the type of the last treasure present in the current vertex
     * @return
     * A String representing the treasure type
     */
    public String getTreasureType() {
        return treasureType;
    }

    /**
     * This method returns the quantity of treasure present in the current vertex
     * @return
     * A float number representing the treasure quantity
     */
    public float getTreasureQuantity() {
        return treasureQuantity;
    }

    /**
     * A setter for the treasureType attribute
     * @param treasureType
     * the new value of treasureType
     */
    public void setTreasureType(String treasureType) {
        this.treasureType = treasureType;
    }

    /**
     * A setter for the treasureQuantity attribute
     * @param treasureQuantity
     * The new value of treasureQuantity
     */
    public void setTreasureQuantity(float treasureQuantity) {
        this.treasureQuantity = treasureQuantity;
    }

    /**
     * Create a new Vertex with the given id.
     *
     * @param id a (unique) name for the vertex.
     */
    public Vertex(String id) {
        this.id = id;
        this.lastSeen = new Date(0);
        this.lastVisited = new Date(0);
        this.golemStench = false;
    }

    /**
     * Get the vertex name (id).
     *
     * @return a String
     */
    public String getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (o != null && o.getClass().equals(this.getClass())) {
            Vertex v = (Vertex) o;
            return v.getId().equals(getId());
        }
        return false;
    }

    /**
     * This method updates the information of the current vertex (present agent, treasure...) from a given vertex
     * The appropriate information will be updated according the differences between the lastVisited and lastSeen attribute values of the two vertices
     * @param v
     * the vertex from which the current one will be updated
     * @return
     * true if the current vertex has changed
     * false otherwise
     */
    public boolean updateFromVertex(Vertex v)
    {
        boolean result=false;
        if(this.equals(v))
        {
            if(getLastSeen().getTime() < v.getLastSeen().getTime())
            {
                setLastSeen(v.getLastSeen());
                this.golemStench = v.golemStench;
                this.setPresentAgent(v.presentAgent);
                result = true;
            }
            if(getLastVisited().getTime() < v.getLastVisited().getTime())
            {
                //treasures....,
                setLastVisited(v.getLastVisited());
                this.golemStench = v.golemStench;
                this.setTreasure(v.treasureType, v.treasureQuantity);
                result = true;
            }
            return result;
        }
        return false;
    }

    /**
     * A setter for both treasureType and treasureQuantity attribute
     * @param treasureType
     * the new treasureType value
     * @param treasureQuantity
     * the new treasureQuantity value
     */
    public void setTreasure(String treasureType, float treasureQuantity)
    {
        this.setTreasureQuantity(treasureQuantity);
        this.setTreasureType(treasureType);
    }



    public void setPresentAgent(AgentDescription agent)
    {
        this.presentAgent = agent;
    }

    /**
     * A setter for all the information of the vertex (treasure and presentAgent)
     * @param agent
     * the new presentAgent
     * @param treasureType
     * the new treasureType value
     * @param treasureQuantity
     * the new treasureQuantity value
     */
    public void setInfo(AgentDescription agent, String treasureType, int treasureQuantity)
    {
        this.setTreasure(treasureType, treasureQuantity);
        this.setPresentAgent(agent);
    }

    public AgentDescription getPresentAgentDescription()
    {
        return this.presentAgent;
    }

    public Class getPresentAgentClass()
    {
        if(this.presentAgent != null)
        {
            return presentAgent.getAgentClass();
        }
        return null;
    }

    public String getPresentAgentTreasureType()
    {
        if(this.presentAgent != null)
        {
            return presentAgent.getTreasureType();
        }
        return null;
    }

    public String getPresentAgentName()
    {
        if(this.presentAgent != null)
        {
            return presentAgent.getName();
        }
        return null;
    }

    /**
     * This method computes the hash code of the current vertex (which is the hash code of its id string)
     * @return
     * The hash code of the vertex
     */
    @Override
    public int hashCode() {
        return this.id.hashCode();
    }

    /**
     * An override of the toString method
     * just returns the id of the vertex
     * @return
     * the string representation of the vertex which is consisting of its id
     */
    @Override
    public String toString() {
        return getId();
    }


    public boolean isGolemStench() {
        return golemStench;
    }

    public void setGolemStench(boolean golemStench) {
        this.golemStench = golemStench;
    }

    public static Set<Vertex> removeVerticesWithStench(Set<Vertex> vertexSet)
    {
        HashSet<Vertex> result = new HashSet<>();
        for(Vertex vertex : vertexSet)
        {
            if(!vertex.isGolemStench())
            {
                result.add(vertex);
            }
        }
        return result;
    }

    public static boolean containsStench(Iterable<Vertex> vertices)
    {
        for(Vertex vertex : vertices)
        {
            if(vertex.isGolemStench())
            {
                return true;
            }
        }
        return false;
    }

    public static Set<Vertex> getContainingStench(Iterable<Vertex> vertices)
    {
        Set<Vertex> result = new HashSet<>();
        for(Vertex vertex : vertices)
        {
            if(vertex.isGolemStench())
            {
                result.add(vertex);
            }
        }
        return result;
    }
}

