package interfaces;

/**
 * This interfaces serves to react when the ReceiveObjectBehaviour receives an object
 * @param <T>
 */
public interface ReceiveObjectHandler<T> {

    public void handle(T receivedObject);
}
