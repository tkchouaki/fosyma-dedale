package log;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * This class serves a logger
 */
public class Log {
    private static Logger log;
    private static FileHandler fh;

    private static boolean isInit = false;

    /**
     * This method writes a string in the log
     * @param message
     * The message to write
     */
    public static void write(String message)
    {
        if(!isInit)
        {
            init();
        }
        log.info(message);
    }

    /**
     * This method initializes the logger
     */
    private static void init()
    {
        log = Logger.getLogger("logger");
        log.setUseParentHandlers(false);
        try {
            fh = new FileHandler("log.txt");
            log.addHandler(fh);
            fh.setFormatter(new SimpleFormatter());
        } catch (IOException e) {
            e.printStackTrace();
        }
        isInit = true;
    }
}
