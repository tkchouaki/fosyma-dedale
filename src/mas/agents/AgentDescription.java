package mas.agents;

import java.io.Serializable;

public class AgentDescription implements Serializable {
    private String name;
    private Class agentClass;
    private String treasureType;
    private int backPackSize;
    private int treasureQuantity;


    public AgentDescription(String name, Class agentClass, String treasureType, int backPackSize, int treasureQuantity)
    {
        this.name = name;
        this.agentClass = agentClass;
        this.treasureType = treasureType;
        this.backPackSize = backPackSize;
        this.treasureQuantity = treasureQuantity;
    }


    public AgentDescription(String name, Class agentClass)
    {
        this(name, agentClass, "none", 0, 0);
    }

    public AgentDescription(String name, Class agentClass, String treasureType, int backPackSize)
    {
        this(name, agentClass, treasureType, backPackSize, 0);
    }


    public String getName() {
        return name;
    }

    public Class getAgentClass() {
        return agentClass;
    }

    public String getTreasureType() {
        return treasureType;
    }

    public int getBackPackSize() {
        return backPackSize;
    }

    public int getTreasureQuantity() {
        return treasureQuantity;
    }


    public boolean emptyTreasure()
    {
        if(this.treasureQuantity>0)
        {
            this.treasureQuantity = 0;
            return true;
        }
        return false;
    }

    public boolean addTreasure(int quantity)
    {
        if(this.treasureQuantity + quantity <= backPackSize)
        {
            this.treasureQuantity+=quantity;
            return true;
        }
        return false;
    }

    public boolean concernsSameAgent(AgentDescription agentDescription)
    {
        return agentDescription.name.equals(this.name);
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj instanceof AgentDescription)
        {
            AgentDescription agentDescription = (AgentDescription) obj;
            return agentDescription.agentClass.equals(this.agentClass) &&
                    agentDescription.name.equals(this.name) &&
                    this.treasureType.equals(agentDescription.treasureType) &&
                    this.treasureQuantity == agentDescription.treasureQuantity &&
                    this.backPackSize == agentDescription.backPackSize;
        }
        return false;
    }
}
