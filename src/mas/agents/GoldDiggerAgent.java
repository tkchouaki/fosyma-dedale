package mas.agents;

import mas.behaviours.GoldDiggerStrategyBehaviour;

/**
 * The GoldDigger agent is designed to pick up treasure in the Maze. It is a simple PirateAgent with a custom Behaviour.
 * The agent's behaviour is described in the GoldDiggerStrategyBehaviour.
 */
public class GoldDiggerAgent extends PirateAgent {

    @Override
    public AgentDescription buildAgentDescription() {
        return new AgentDescription(this.getLocalName(), this.getClass(), this.getMyTreasureType(), this.getBackPackFreeSpace(), 0);
    }

    @Override
    public void setup()
    {
        super.setup();
        this.addBehaviour(new GoldDiggerStrategyBehaviour(this));
    }
}
