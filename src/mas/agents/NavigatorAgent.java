package mas.agents;

import mas.behaviours.NavigatorStrategyBehaviour;

/**
 * The NavigatorAgent is designed to explore the maze. It is a simple PirateAgent with a custom behaviour.
 * The agent's behaviour is described in the NavigatorStrategyBehaviour.
 */
public class NavigatorAgent extends PirateAgent {

    @Override
    public AgentDescription buildAgentDescription() {
        return new AgentDescription(this.getLocalName(), this.getClass(), "none", -1, 0);
    }

    @Override
    public void setup()
    {
        super.setup();
        // Add the behaviour
        this.addBehaviour(new NavigatorStrategyBehaviour(this));
    }
}
