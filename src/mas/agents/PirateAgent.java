package mas.agents;

import env.EntityType;
import env.Environment;
import graph.Graph;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import mas.abstractAgent;
import mas.behaviours.PeriodicCleaningBehaviour;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;


/**
 * The Pirate Agent is the base class for all our agents. It have its own graph. For convenience, all PirateAgents are
 * registered to the DF service. The provided name is the agent's name, and the proposed service is its class name. When
 * a PirateAgent is destroyed, it will be automatically removed from the DF service.
 */
public abstract class PirateAgent extends abstractAgent {

    /**
     * The agent's graph.
     */
    private Graph graph;


    /**
     * Get the agent's graph. Be aware that it is not a copy, so any update on the graph will impact the agent.
     * @return a Graph instance
     */
    public Graph getGraph() {
        return graph;
    }


    public abstract AgentDescription buildAgentDescription();

    @Override
    public void setup()
    {
        super.setup();

        //get the parameters given into the object[]. In the current case, the environment where the agent will evolve
        final Object[] args = getArguments();
        if (args[0]!=null && args[1]!=null) {
            deployAgent((Environment) args[0], (EntityType) args[1]);
        } else {
            System.err.println("Malfunction during parameter's loading of agent"+ this.getClass().getName());
            System.exit(-1);
        }

        //Register agent type and offered services to the DF
        DFAgentDescription dfd = new DFAgentDescription();
        ServiceDescription sd = new ServiceDescription();
        if(this instanceof StewardAgent)
        {
            sd.setType("silo");
        }
        else
        {
            sd.setType(this.getClass().getSimpleName());
        }
        sd.setName(getLocalName());
        dfd.addServices(sd);
        try
        {
            DFService.register(this, dfd);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }
        // Create the graph structure.
        this.graph = new Graph(getLocalName(), this.buildAgentDescription());
        this.addBehaviour(new PeriodicCleaningBehaviour(this));
    }

    @Override
    protected void takeDown(){
        // Agent is going to be deleted so it should be removed from the DF.
        try {
            DFService.deregister(this);
        } catch (FIPAException e) {
            e.printStackTrace();
        }
    }

}
