package mas.agents;

import mas.behaviours.StewardStrategyBehaviour;


/**
 * The StewardAgent is designed to be a tanker agent. It is a simple PirateAgent with a custom Behaviour. See the
 * StewardStrategyBehaviour if you want to know how will the agent evolve in the Maze.
 */
public class StewardAgent extends PirateAgent {

    @Override
    public AgentDescription buildAgentDescription() {
        return new AgentDescription(this.getLocalName(), this.getClass(), "all", -2, 0);
    }

    @Override
    public void setup()
    {
        super.setup();
        this.addBehaviour(new StewardStrategyBehaviour(this));
    }
}
