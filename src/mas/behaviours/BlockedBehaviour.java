package mas.behaviours;

import graph.Couple;
import graph.DistCoupleComparator;
import graph.Graph;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import mas.agents.PirateAgent;

import java.io.IOException;
import java.util.*;


/**
 *
 */
public class BlockedBehaviour extends CommunicationBehaviour {

    public static final int MAX_TTL = 500;

    /**
     *
     * @param myAgent
     */
    public BlockedBehaviour(PirateAgent myAgent) {
        super(myAgent);
    }

    /**
     *
     * @param myAgent
     * @param parent
     */
    public BlockedBehaviour(PirateAgent myAgent, PirateBehaviour parent) {
        super(myAgent, parent);
    }

    /**
     *
     */
    @Override
    public void action() {
        List<String> nearbyAgents = getFellowAgentsList();
        String conversationID = new Date().toString() + new Random().nextInt();
        PirateAgent agent = (PirateAgent) getAgent();

        // A random TTL to be sure that two blocked agent won't try to communicate at the same time
        int random_ttl = new Random().nextInt(MAX_TTL - 1);
        Date ttl = new Date(new Date().getTime() + random_ttl);

        PirateMessage helloBlocked = new SimpleMessage(
                nearbyAgents,
                conversationID,
                ttl,
                "hello blocked",
                ACLMessage.INFORM
        );
        MessageTemplate replyTemplate = MessageTemplate.MatchConversationId(conversationID);

        List<ACLMessage> replies = sendMessageAndWaitReplies(helloBlocked, replyTemplate);
        List<String> blockedAgents = new ArrayList<>();

        // Treat replies
        for (ACLMessage reply : replies) {
            if (reply.getReplyByDate().getTime() > ttl.getTime()) {
                continue; // TTL exceeded, message ignored
            }
            blockedAgents.add(reply.getSender().getLocalName());

            Graph peerGraph = null;
            // Get the peerAgent Graph
            try {
                peerGraph = (Graph) reply.getContentObject();
            } catch (UnreadableException e) {
                // Do nothing for now, just skips the graph union
                continue;
            }
            agent.getGraph().addInfo(peerGraph);
        }

        ttl = new Date(new Date().getTime() + MAX_TTL);
        // We are sending the updated graph to other agents
        try {
            PirateMessage graphMessage = new SimpleMessage(
                    blockedAgents,
                    conversationID,
                    ttl,
                    agent.getGraph(),
                    ACLMessage.INFORM
            );
            replyTemplate = MessageTemplate.MatchConversationId(conversationID);
            replies = sendMessageAndWaitReplies(graphMessage, replyTemplate);

        } catch (IOException e) {
            PirateMessage cancelMessage = new SimpleMessage(
                    blockedAgents,
                    conversationID,
                    ttl,
                    "error",
                    ACLMessage.FAILURE
            );
            agent.sendMessage(cancelMessage);
            return;
        }

        Queue<Couple<String, Integer>> priorities = new PriorityQueue<>(new DistCoupleComparator<>());
        // Compute here the agent priority
        for (ACLMessage reply : replies) {
            if (reply.getReplyByDate().getTime() > ttl.getTime()) {
                continue;
            }

            String peerAgent = reply.getSender().getLocalName();
            Integer priority = Integer.valueOf(reply.getContent());
            priorities.add(new Couple<>(peerAgent, priority));
        }

    }
}
