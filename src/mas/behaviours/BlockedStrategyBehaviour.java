package mas.behaviours;

import graph.Vertex;
import mas.agents.PirateAgent;

import java.util.List;
import java.util.Set;

public class BlockedStrategyBehaviour extends StrategyBehaviour{

    private boolean startedMoving;

    public BlockedStrategyBehaviour(PirateAgent myAgent, PirateBehaviour parent, List<Vertex> targets) {
        super(myAgent, parent);
        this.startedMoving = false;
    }


    public List<Vertex> computeNextPath() {
        return null;
    }

    @Override
    public List<PirateBehaviour> generateStep(Vertex target) {
        return null;
    }

    @Override
    public List<PirateBehaviour> generateStep() {
        return null;
    }

    @Override
    public Set<Vertex> getTargets() {
        return null;
    }
}
