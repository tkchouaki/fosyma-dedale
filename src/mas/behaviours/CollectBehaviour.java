package mas.behaviours;


import log.Log;
import mas.agents.GoldDiggerAgent;
import mas.agents.PirateAgent;

/**
 * This behaviour is designed to let GoldDigger Agents collect treasures and diamonds.
 * This is a OneShot behaviour, and it will try to pick the treasure only one time.
 */
public class CollectBehaviour extends OnSpotBehaviour {

    /**
     * A Constructor taking a GoldDiggerAgent
     * @param myAgent a GoldDiggerAgent
     */
    public CollectBehaviour(GoldDiggerAgent myAgent) {
        super(myAgent);
    }

    /**
     * A Constructor taking a GoldDiggerAgent and a parent PirateBehaviour
     * @param myAgent a GoldDiggerAgent
     * @param parent a PirateBehaviour
     */
    public CollectBehaviour(GoldDiggerAgent myAgent, PirateBehaviour parent) {
        super(myAgent, parent);
    }

    /**
     * The behaviour is just going to pick the treasure or diamonds on the agent's current position.
     * Will notify parent if it's a success or a failure.
     */
    @Override
    public void action() {
        PirateAgent pirateAgent = getAgent();
        int picked = pirateAgent.pick();
        Log.write(pirateAgent.getLocalName() + " collected " + picked);
        setSuccess(picked != 0);
        pirateAgent.getGraph().addToTreasure(picked);
        setFinished(true);
    }
}
