package mas.behaviours;

import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import mas.agents.PirateAgent;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * This class serves as a framework for communication between agents
 * It provides basic functionality such as getting agents list and sending messages with waiting for replies
 */
abstract public class CommunicationBehaviour extends OnSpotBehaviour {

    protected static final int TTL=250;

    /**
     * A constructor taking a PirateAgent
     * @param myAgent
     */
    public CommunicationBehaviour(PirateAgent myAgent) {
        super(myAgent);
    }

    /**
     *
     * A constructor taking a PirateAgent and a parent PirateBehaviour
     * @param myAgent
     * @param parent
     */
    public CommunicationBehaviour(PirateAgent myAgent, PirateBehaviour parent) {
        super(myAgent, parent);
    }

    /**
     * A method that sends a list of messages and then wait for their replies
     * It's different from calling sendMessageAndWaitReplies in a loop because this one sends all the messages and then
     * waits until the maximum replyByDate is over
     * @param messages
     * a List of messages
     * @param repliesTemplates
     * a List of replyTemplates
     * @return
     * a List of replies
     */
    protected List<ACLMessage> sendMessagesAndWaitReplies(List<ACLMessage> messages, List<MessageTemplate> repliesTemplates) {
        PirateAgent agent = (PirateAgent) getAgent();
        Date endDate = new Date();
        MessageTemplate bigTemplate = null;

        // Let's iterate over each message and compute the biggest TTL. At the same time, messages are sent.
        for(int i=0; i<messages.size(); i++)
        {

            ACLMessage message = messages.get(i);
            if(endDate.getTime() < message.getReplyByDate().getTime())
            {
                endDate = message.getReplyByDate();
            }
            if(i==0)
            {
                bigTemplate = repliesTemplates.get(i);
            }
            else
            {
                bigTemplate = MessageTemplate.or(bigTemplate, repliesTemplates.get(i));
            }
            agent.sendMessage(message);
        }
        bigTemplate = MessageTemplate.and(bigTemplate, MessageTemplate.MatchPerformative(ACLMessage.INFORM));
        List<ACLMessage> replies = new ArrayList<>();
        Date now = new Date();
        while (endDate.getTime() - now.getTime() > 0)
        {
            ACLMessage reply = agent.blockingReceive(bigTemplate, endDate.getTime() - now.getTime());
            if (reply != null)
            {
                replies.add(reply);
            }
            now = new Date();
        }
        return replies;
    }
}
