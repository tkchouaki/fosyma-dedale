package mas.behaviours;

import env.Attribute;
import graph.Graph;
import graph.Vertex;
import mas.agents.GoldDiggerAgent;
import mas.agents.PirateAgent;
import util.Util;

import java.util.*;


/**
 * The GoldDiggerStrategy is the main strategy for GoldDigger agents. The agent will :
 *      - try to get to the closest 2 biggest treasure according to its treasure type
 *      - act like a Navigator agent if no treasure are found
 *      - empty its backpack if the maze is completely discovered
 * To do the later one, it will compute the neighbours of the central point and reach one of them.
 */
public class GoldDiggerStrategyBehaviour extends StrategyBehaviour {

    /**
     * @param myAgent
     */
    public GoldDiggerStrategyBehaviour(PirateAgent myAgent) {
        super(myAgent);
    }

    /**
     * @param target The target vertex
     * @return
     */
    @Override
    public List<PirateBehaviour> generateStep(Vertex target) {
        List<PirateBehaviour> result = generateStep();
        result.add(0, new MoveBehaviour(getAgent(), this, target.getId()));
        return result;
    }


    /**
     * @return
     */
    @Override
    public List<PirateBehaviour> generateStep() {
        PirateAgent agent = getAgent();
        Vertex position = getGraph().getCurrentPosition();

        List<PirateBehaviour> result = new ArrayList<>();
        result.add(new SendGraphBehaviour(agent, this));
        result.add(new ReceiveGraphBehaviour(agent, this));

        Collections.shuffle(result);
        result.add(0, new ObserveBehaviour(agent, this));
        if (position != null && position.getTreasureType() != null && position.getTreasureType().equals(agent.getMyTreasureType()) && position.getTreasureQuantity() > 0) {
            result.add(0, new CollectBehaviour((GoldDiggerAgent) agent, this));
        }
        result.add(new StockBehaviour(agent, this));
        return result;
    }


    @Override
    public void action()
    {
        super.action();
    }

    /**
     * The targets of the GoldDigger agent will as following :
     * he would like to go to the two vertices containing the mose amount of treasure that the agent can pick
     * otherwise, just act like a navigator agent
     * if still no target, he'll go to empty his backpack
     * @return
     * A set of target vertices
     */
    @Override
    public Set<Vertex> getTargets()
    {
        GoldDiggerAgent myPirateAgent = (GoldDiggerAgent) this.myAgent;
        Graph graph = this.getGraph();
        List<Vertex> targets = new ArrayList<>();
        Set<Vertex> temp;
        Set<Vertex> discoveredVertices;

        // Computes all nodes with my treasure type
        if (myPirateAgent.getBackPackFreeSpace() >= 5)
        {
            if (myPirateAgent.getMyTreasureType().equals(Attribute.TREASURE.getName()))
            {
                targets.addAll(graph.getWithTreasureVertices());
            }
            else if (myPirateAgent.getMyTreasureType().equals(Attribute.DIAMONDS.getName()))
            {
                targets.addAll(graph.getWithDiamondsVertices());
            }

            temp = graph.getVerticesImClosestToAmongAgentsWithSameClassAndTreasureType(new HashSet<>(targets));
            if(temp.size() > 0)
            {
                targets = new ArrayList<>(temp);
            }

            // We want to keep the 2 best one (with the higher amount of treasure/diamonds)
            targets.sort(new Comparator<Vertex>() {
                @Override
                public int compare(Vertex o1, Vertex o2)
                {
                    return Float.compare(o1.getTreasureQuantity(), o2.getTreasureQuantity());
                }
            });
            Collections.reverse(targets);
            for (int i = 0; i < targets.size() - 2; ++i)
            {
                targets.remove(2);
            }
        }

        // No targets ? Let's try to act like a discoverer agent
        if (targets.size() == 0)
        {
            discoveredVertices = graph.getDiscoveredVertices();
            temp = graph.getVerticesImClosestTo(discoveredVertices);
            if(temp.size() == 0)
            {
                targets.addAll(discoveredVertices);
            }
            else
            {
                targets.addAll(temp);
            }
        }

        if (targets.size() == 0)
        {
            Vertex centralPoint = graph.getCentralPoint();
            // Still no targets ? Well, we need to empty our backpack if we have a treasure
            if (graph.getTreasureQuantity()>0)
            {
                targets.addAll(graph.getNeighbours(centralPoint));
            }
            else
            {
                List<Set<Vertex>> levels = graph.getAccessibilityTreeFromVertex(centralPoint, -1);
                for(int i=1; i<levels.size() && targets.size()<5; i++)
                {
                    for(Vertex v : levels.get(levels.size()-i))
                    {
                        temp = new HashSet<>();
                        temp.add(v);
                        if(v.getTreasureQuantity() == 0 && graph.getNeighbours(v).size()!=2 && !graph.getPath(graph.getCurrentPosition(), temp).contains(centralPoint))
                        {
                            targets.add(v);
                        }
                    }

                }

            }
        }

        Set<Vertex> result = new HashSet<>(targets);


        // One of our targets in exactly where I am ? Well no targets then.
        /*if (targets.contains(this.getGraph().getCurrentPosition()))
        {
            targets = new ArrayList<>();
        }*/
        return result;
    }
}