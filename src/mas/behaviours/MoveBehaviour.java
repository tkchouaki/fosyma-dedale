package mas.behaviours;

import graph.Vertex;
import log.Log;
import mas.agents.PirateAgent;

/**
 * This behaviour is describing how agents are moving in the Maze. Given a target, it will try to move to that target.
 * Several trials will be performed (see MAX_TRAILS) before being in failure.
 * This is a OneShot behaviour.
 */
public class MoveBehaviour extends PirateBehaviour {

    /**
     * How many trials before being in failure.
     */
    public static final int MAX_TRIALS = 3;

    private static final int SLEEP_TIME = 100;

    private String target;
    private int nbTrials;

    /**
     * Create a new MoveBehaviour with the provided target for the provide agent.
     * @param myAgent a PirateAgent that will execute the move
     * @param target the target (a node in the graph)
     */
    public MoveBehaviour(PirateAgent myAgent, String target)
    {
        super(myAgent);
        this.target = target;
        this.nbTrials=0;
    }

    /**
     * Same as the first constructor, but the MoveBehaviour is declared as a child for the provided PirateBehaviour.
     * Success and failure will be notified to the parent behaviour (see setSuccess(boolean) in PirateBehaviour)
     * @param myAgent a PirateAgent
     * @param parent a PirateBehaviour
     * @param target the target
     */
    public MoveBehaviour(PirateAgent myAgent, PirateBehaviour parent, String target)
    {
        super(myAgent, parent);
        this.target = target;
        this.nbTrials=0;
    }

    /**
     * Perform the move.
     * It will try at most MAX_TRIALS times and will notify the parent if provided.
     */
    @Override
    public void action()
    {
        if(nbTrials==0)
        {
            try {
                Thread.sleep(SLEEP_TIME);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        boolean couldMove = getAgent().moveTo(target);
        this.nbTrials++;
        if(couldMove)
        {
            this.setSuccess(true);
            this.getGraph().setCurrentPosition(new Vertex(target));
            this.setFinished(true);
        }
        else
        {
            Log.write(this.myAgent.getLocalName() + " Failed to go to " + this.target + " for the " + this.nbTrials + " time");
            if(this.nbTrials==MAX_TRIALS)
            {
                Log.write(this.myAgent.getLocalName() + " Failed to go " + this.target);
                this.setSuccess(false);
                this.setFinished(true);
            }
            // The move behaviour will be re-run because the finnish flag hasn't been set
        }
    }

    /**
     * Get the provided target.
     * @return a String representing the node's name.
     */
    public String getTarget() {
        return target;
    }

    @Override
    public String toString()
    {
        return "MoveBehaviour to " + this.target;
    }
}
