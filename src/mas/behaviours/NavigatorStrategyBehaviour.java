package mas.behaviours;

import graph.Graph;
import graph.Vertex;
import mas.agents.NavigatorAgent;
import mas.agents.PirateAgent;
import util.Util;

import java.util.*;

/**
 *  This class describes the strategy followed by navigator agents
 *      -If the map is not fully explored yet, the targets will be the the unvisited vertices
 *      -Otherwise, the agent will want to update his information by going to the oldest visited vertices
 *  On each moving step, the agent will observe the current vertex and then try to exchange his graph
 */
public class NavigatorStrategyBehaviour extends StrategyBehaviour {

    public NavigatorStrategyBehaviour(PirateAgent myAgent) {
        super(myAgent);
    }


    /**
     * On each moving step, the agent will observe the current vertex and then try to exchange his graph
     * @param target
     * The target vertex
     * @return
     * A list of behaviours
     */
    @Override
    public List<PirateBehaviour> generateStep(Vertex target)
    {
        List<PirateBehaviour> result = generateStep();
        PirateAgent agent = (PirateAgent) getAgent();
        result.add(0, new MoveBehaviour(agent, this, target.getId()));
        return result;
    }

    /**
     * Every time the agent will try to update his graph after having observed the current vertex
     * @return
     * A list of behaviours
     */
    @Override
    public List<PirateBehaviour> generateStep() {
        List<PirateBehaviour> result = new ArrayList<>();
        PirateAgent agent = (PirateAgent) getAgent();
        result.add(new SendGraphBehaviour(agent, this));
        result.add(new ReceiveGraphBehaviour(agent, this));
        Collections.shuffle(result);
        result.add(0, new ObserveBehaviour(agent, this));
        return result;
    }

    /**
     * If the map is not fully explored yet, the navigator agent will go to the unvisited vertices
     * @return
     * A set of target vertices
     */
    @Override
    public Set<Vertex> getTargets() {
        Graph graph = this.getGraph();
        Set<Vertex> targets;
        Set<Vertex> discovered = graph.getDiscoveredVertices();
        //return discovered vertices if not empty
        if(discovered.size()>0)
        {
            targets = graph.getVerticesImClosestTo(discovered);
            if(targets.size() == 0)
            {
                targets = discovered;
            }
        }
        //return the oldest explored vertex otherwise
        else
        {
            targets = new HashSet<>();
            Vertex oldestVertex = graph.getOldestExploredVertex();
            if(this.myAgent.getLocalName().equals(graph.getClosestAgentTo(oldestVertex, this.myAgent.getClass())))
            {
                targets.add(oldestVertex);
            }
            else
            {
                Set<Vertex> verticesWithoutAgents = graph.getVertices();
                verticesWithoutAgents.removeAll(graph.getVerticesWithAgents());
                targets.addAll(verticesWithoutAgents);
            }
        }
        return targets;
    }
}