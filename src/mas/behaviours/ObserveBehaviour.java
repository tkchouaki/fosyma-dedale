package mas.behaviours;

import env.Attribute;
import env.Couple;
import graph.Graph;
import graph.Vertex;
import mas.agents.PirateAgent;

import java.util.Date;
import java.util.List;

/**
 * This behaviour implements the observing of the current vertex by a PirateAgent
 * It update's the agent information about the environment
 */
public class ObserveBehaviour extends OnSpotBehaviour {

    public ObserveBehaviour(PirateAgent myAgent) {
        super(myAgent);
    }

    public ObserveBehaviour(PirateAgent myAgent, PirateBehaviour parent) {
        super(myAgent, parent);
    }

    @Override
    public void action() {
        PirateAgent myPirateAgent = (PirateAgent) this.myAgent;

        Graph graph = this.getGraph();

        // Get the agent's current position
        String myPosition=myPirateAgent.getCurrentPosition();

        if (!myPosition.equals(""))
        {
            Date now = new Date();

            //List of observable from the agent's current position
            List<Couple<String,List<Attribute>>> lobs = myPirateAgent.observe();

            // Transform the current location in vertex and add it to the graph
            Vertex current = new Vertex(myPosition);
            current.setLastVisited(now);

            boolean setGolemStench = false;
            // Update the agent's graph
            for(Attribute attribute : lobs.get(0).getRight())
            {
                if(attribute.equals(Attribute.TREASURE) || attribute.equals(Attribute.DIAMONDS))
                {
                    current.setTreasure(attribute.getName(), (Integer) attribute.getValue());
                }
                if(attribute.equals(Attribute.STENCH))
                {
                    current.setGolemStench(true);
                    setGolemStench = true;
                }
            }
            if(!setGolemStench)
            {
                current.setGolemStench(false);
            }
            graph.addVertex(current);
            graph.setCurrentPosition(current);
            //the current vertex is explored and no longer discovered
            graph.addExploredVertex(current);
            graph.removeDiscoveredVertex(current);

            //add adjacent vertices
            for (int i = 1; i < lobs.size(); i++)
            {
                Vertex neighbour = new Vertex(lobs.get(i).getLeft());
                setGolemStench = false;
                for(Attribute attribute : lobs.get(i).getRight())
                {
                    if(attribute.equals(Attribute.STENCH))
                    {
                        neighbour.setGolemStench(true);
                        System.out.println("Stench at "  + neighbour.getId());
                        setGolemStench=true;
                    }
                }
                if(!setGolemStench)
                {
                    neighbour.setGolemStench(false);
                }
                neighbour.setLastSeen(now);
                graph.addEdge(current, neighbour);
                if (!graph.isVertexExplored(neighbour)) graph.addDiscoveredVertex(neighbour);
            }

            // Set the vertices' classes to display them with the right color
            graph.updateStyles();
        }

        this.setFinished(true);
    }
}
