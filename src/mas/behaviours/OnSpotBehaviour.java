package mas.behaviours;

import mas.agents.PirateAgent;

/**
 * This class serves as a base class for Behaviours that takes place in one spot
 * We can assume that the agent does not move while an OnSpotBehaviour is running
 */
public abstract class OnSpotBehaviour extends PirateBehaviour {

    public OnSpotBehaviour(PirateAgent myAgent) {
        super(myAgent);
    }

    public OnSpotBehaviour(PirateAgent myAgent, PirateBehaviour parent) {
        super(myAgent, parent);
    }
}
