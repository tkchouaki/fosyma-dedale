package mas.behaviours;

import graph.Graph;
import graph.Vertex;
import jade.core.behaviours.TickerBehaviour;
import mas.agents.PirateAgent;

public class PeriodicCleaningBehaviour extends TickerBehaviour {


    public PeriodicCleaningBehaviour(PirateAgent myAgent)
    {
        super(myAgent, 10000);
    }

    @Override
    protected void onTick() {
        Graph graph = ((PirateAgent) this.myAgent).getGraph();
        for(Vertex v : graph.getVertices())
        {
            v.setPresentAgent(null);
            v.setGolemStench(false);
        }
    }
}
