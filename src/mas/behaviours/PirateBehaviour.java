package mas.behaviours;

import graph.Graph;
import jade.core.behaviours.SimpleBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import log.Log;
import mas.agents.PirateAgent;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is design to factorize some common code : it adds a simple way to finish a Behaviour (setFinished).
 * It is also retrieving the agent's graph for convenience.
 */
public abstract class PirateBehaviour extends SimpleBehaviour{
    private boolean finished;
    private Graph graph;

    private PirateBehaviour parent;

    private boolean success;
    private boolean hasLastChildSucceeded;

    /**
     * Create a new PirateBehaviour with the given agent.
     * @param myAgent the agent you want to attach the behaviour
     */
    public PirateBehaviour(PirateAgent myAgent)
    {
        super(myAgent);
        this.finished = false;
        this.graph = myAgent.getGraph();
        this.success = true;
        this.hasLastChildSucceeded = true;
    }

    /**
     * Create a new PirateBehaviour as a child of an other PirateBehaviour.
     * @param myAgent the agent you want to attach the behaviour
     * @param parent the parent PirateBehaviour
     */
    public PirateBehaviour(PirateAgent myAgent, PirateBehaviour parent)
    {
        this(myAgent);
        this.parent = parent;
    }

    /**
     * Set the flag finished to the specified value. If your action method returns and the flags is set to true, the
     * behaviour is destroyed.
     * If this behaviour has been declared as a child & it has finished, the parent will be restarted
     * @param finished the new flag's value
     */
    protected void setFinished(boolean finished)
    {
        this.finished = finished;
        if(this.finished && parent != null)
        {
            this.parent.setHasLastChildSucceeded(this.success);
            Log.write(this.toString() + " going to restart " + this.parent.toString());
            this.parent.restart();
        }

    }

    /**
     * A convenient way to get the agent graph. Same as ((PirateAgent) getAgent()).getGraph()
     * @return the agent's Graph
     */
    protected Graph getGraph()
    {
        return this.graph;
    }

    @Override
    /**
     * Called when the action method returns. If the flag finished is set to true, the behaviour will be destroyed.
     */
    public final boolean done()
    {
        return this.finished;
    }

    /**
     * The success flag will be set to the provided value. It is used when the behaviour is a child of an other
     * one.
     * @param success the new success flag's value
     */
    protected void setSuccess(boolean success)
    {
        this.success = success;
    }


    /**
     * Get the current value of the success flag
     * @return the current value
     */
    protected boolean getSuccess()
    {
        return this.success;
    }

    /**
     * A convenient method for a blocking receive given a MessageTemplate and a ttl.
     * @param template the reply MessageTemplate
     * @param ttl time-to-live : how long should the behaviour wait ?
     * @return an ACLMessage or null is there is no match given the time and the template
     */
    final protected ACLMessage waitMessage(MessageTemplate template, int ttl)
    {
        return this.myAgent.blockingReceive(template, ttl);
    }

    /**
     * Set the flag hasLastChildSucceeded value to the provided one.
     * @param hasLastChildSucceeded the new value
     */
    private void setHasLastChildSucceeded(boolean hasLastChildSucceeded)
    {
        this.hasLastChildSucceeded = hasLastChildSucceeded;
    }

    /**
     * A method that retrieves the list of PirateAgents in the environment
     * @return
     * A List containing agents name
     */
    public List<String> getFellowAgentsList() {
        return getFellowAgentsList(null);
    }


    /**
     * A method that retrieves the list of PirateAgents in the environment
     * @return
     * A List containing agents name
     */
    public List<String> getFellowAgentsList(String agentType) {
        DFAgentDescription template = new DFAgentDescription();
        if(agentType != null)
        {
            ServiceDescription sd = new ServiceDescription();
            sd.setType(agentType);
            template.addServices(sd);
        }
        List<String> nearbyAgents = new ArrayList<>();


        try {
            // Build the list of nearby agents
            DFAgentDescription[] result = DFService.search(getAgent(), template);
            for (DFAgentDescription agentDescription : result) {
                nearbyAgents.add(agentDescription.getName().getLocalName());
            }
            // Remove the agent itself
            nearbyAgents.remove(getAgent().getLocalName());

        } catch (FIPAException e) {
            // Something bad happened
            e.printStackTrace();
            setSuccess(false);
            setFinished(true);
        }
        return nearbyAgents;
    }

    /**
     * A convenient method to get the Agent as an instance of PirateAgent
     * @return a PirateAgent
     */
    @Override
    public PirateAgent getAgent() {
        return (PirateAgent) super.getAgent();
    }

    /**
     * Send a message and wait replies. The given message should provide :
     *  - the replyByDate field (used as a time-to-live)
     *  - the receivers
     *  - the conversation id
     *  - the performative
     * The provided MessageTemplate will be used to filter replies, so it needs contain:
     *  - the conversation id
     *  - the performative
     *
     * @param message the message that will be sent
     * @param replyTemplate the reply format
     * @return the list of different received replies
     */
    protected List<ACLMessage> sendMessageAndWaitReplies(ACLMessage message, MessageTemplate replyTemplate) {
        PirateAgent agent = (PirateAgent) this.myAgent;
        Date endDate = message.getReplyByDate();

        // Send message to agents
        agent.sendMessage(message);

        // Setup some variables
        List<ACLMessage> replies = new ArrayList<>();
        Date now = new Date();

        // Wait until the ttl has been reached
        while (endDate.getTime() - now.getTime() > 0)
        {
            ACLMessage reply = agent.blockingReceive(replyTemplate, endDate.getTime() - now.getTime());
            if (reply != null)
            {
                replies.add(reply);
            }
            now = new Date();
        }
        return replies;
    }

    protected void emptyMail()
    {
        MessageTemplate template = MessageTemplate.MatchAll();
        while(this.myAgent.receive(template)!=null);
    }

    protected void emptyMail(MessageTemplate template)
    {
        while(this.myAgent.receive(template)!=null);
    }
}
