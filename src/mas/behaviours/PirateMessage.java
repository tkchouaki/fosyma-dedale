package mas.behaviours;

import jade.core.AID;
import jade.lang.acl.ACLMessage;

import java.util.Date;

/**
 * The PirateMessage class is designed to provided a little wrapper around Jade Message API. It provides two
 * constructors that will build a message with the given parameters.
 */
public abstract class PirateMessage extends ACLMessage {

    /**
     * Create a new PirateMessage with the provided parameters.
     * @param agents an Iterable of Strings, wich are the agent's local name.
     * @param conversationID a unique conversation id
     * @param ttl time-to-live. Message should be ignored if read after the provided Date.
     * @param performative a performative, as defined in FIPA
     */
    public PirateMessage(Iterable<String> agents, String conversationID, Date ttl, int performative) {
        super(performative);
        for (String agent : agents) {
            this.addReceiver(new AID(agent, AID.ISLOCALNAME));
        }
        this.setConversationId(conversationID);
        if (ttl != null) {
            this.setReplyByDate(ttl);
        }
    }

    /**
     * Create a new PirateMessage with the provided parameters.
     * @param agents an Iterable of Strings, wich are the agent's local name.
     * @param conversationID a unique conversation id
     * @param ttl time-to-live. Message should be ignored if date is now + ttl (sec)
     * @param performative a performative, as defined in FIPA
     */
    public PirateMessage(Iterable<String> agents, String conversationID, int ttl, int performative) {
        super(performative);
        for (String agent : agents) {
            this.addReceiver(new AID(agent, AID.ISLOCALNAME));
        }
        this.setConversationId(conversationID);
        this.setReplyByDate(new Date(new Date().getTime() + ttl));
    }
}
