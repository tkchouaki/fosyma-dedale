package mas.behaviours;

import graph.Graph;
import jade.core.AID;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import log.Log;
import mas.agents.PirateAgent;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This behaviour is designed to send the agent graph when requested and receive back the other agent one.
 */
public class ReceiveGraphBehaviour extends CommunicationBehaviour {

    public ReceiveGraphBehaviour(PirateAgent myAgent) {
        super(myAgent);
    }

    public ReceiveGraphBehaviour(PirateAgent myAgent, PirateBehaviour parent) {
        super(myAgent, parent);
    }

    /**
     * First, check the mailbox to see if the agent received a request. Then, for each matching message, send the graph
     * and wait a reply.
     */
    @Override
    public void action() {
        PirateAgent agent = getAgent();

        // Let's check if we received a hello message
        MessageTemplate helloTemplate = MessageTemplate.and(
                MessageTemplate.MatchContent("hello"),
                MessageTemplate.MatchPerformative(ACLMessage.INFORM)
        );

        // Setup some variables
        List<ACLMessage> ackMessages = new ArrayList<>();
        List<MessageTemplate> templates = new ArrayList<>();

        // Start receiving
        ACLMessage helloMessage = agent.receive(helloTemplate);
        while(helloMessage != null) {

            // Check if we have to reply
            if(helloMessage.getReplyByDate().getTime() >= new Date().getTime()) {

                // Create the appropriate template (for the sender reply)
                MessageTemplate graphTemplate = MessageTemplate.and(
                        MessageTemplate.and(
                                MessageTemplate.MatchConversationId(helloMessage.getConversationId()),
                                MessageTemplate.MatchSender(helloMessage.getSender())
                        ),
                        MessageTemplate.or(
                                MessageTemplate.MatchPerformative(ACLMessage.PROPOSE),
                                MessageTemplate.MatchPerformative(ACLMessage.FAILURE)
                        )
                );
                templates.add(graphTemplate);

                // Create the reply
                ACLMessage ackMessage;
                try {
                    ackMessage = this.buildReply(
                            helloMessage.getSender().getLocalName(),
                            helloMessage.getConversationId()
                    );
                } catch (IOException e) {
                    // We are unable to serialize the graph, at like it is out-of-range and so don't reply
                    continue;
                }
                ackMessages.add(ackMessage);

            }
            // Treat the next message
            helloMessage = agent.receive(helloTemplate);
        }

        if(ackMessages.size()>0)
        {
            List<ACLMessage> replies = this.sendMessagesAndWaitReplies(ackMessages, templates);
            processReplies(replies);
        }
        setSuccess(true);
        setFinished(true);
    }

    private ACLMessage buildReply(String receiverName, String conversationId) throws IOException
    {
        // Setup variables
        PirateAgent agent = getAgent();
        List<String> listReceiver = new ArrayList<>();
        listReceiver.add(receiverName);

        // Create TTL
        Date ttl = new Date(new Date().getTime() + TTL);

        // Create the message with the agent's graph inside
        ACLMessage ackMessage = new SimpleMessage(
                listReceiver,
                conversationId,
                ttl,
                getGraph(),
                ACLMessage.PROPOSE
        );
        ackMessage.setSender(new AID(agent.getLocalName(), AID.ISLOCALNAME));

        // Return result
        return ackMessage;
    }

    private void processReplies(List<ACLMessage> replies) {
        Graph agentGraph = getGraph();
        for(ACLMessage reply : replies) {
            if (reply.getPerformative() == ACLMessage.FAILURE) {
                // Log the failure
                PirateAgent agent = getAgent();
                Log.write(agent.toString()
                        + " warned by "
                        + reply.getSender().getLocalName()
                        + " : "
                        + reply.getContent()
                );
                continue;
            }
            try {
                Graph g = (Graph) reply.getContentObject();
                agentGraph.addInfo(g);
            } catch (UnreadableException e) {
                // Ignore errors
            }

        }
    }
}
