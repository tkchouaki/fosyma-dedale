package mas.behaviours;

import graph.Graph;
import jade.core.AID;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import mas.agents.PirateAgent;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * The behaviour is responsible of querying nearby agents and then exchange graphs between the owner agent and
 * the in range agents.
 * AG ----- "hello" ----> OAG
 * AG <---- graphOA ----- OAG
 * AG ----- graphAG ----> OAG
 */
public class SendGraphBehaviour extends CommunicationBehaviour {

    public SendGraphBehaviour(PirateAgent myAgent) {
        super(myAgent);
    }

    public SendGraphBehaviour(PirateAgent myAgent, PirateBehaviour parent) {
        super(myAgent, parent);
    }

    @Override
    /**
     * Start by sending a simple "hello" message to find nearby agents and then do the exchange.
     */
    public void action() {
        // We are about to send our graph to an other agent
        // Let's say hello to nearby agents
        List<String> nearbyAgents = getFellowAgentsList();

        // Create a new unique conversation ID
        String conversationID = new Date().toString() + new Random().nextInt(1000000);

        // Now let's build the ACK template
        MessageTemplate ackTemplate = MessageTemplate.and(
                MessageTemplate.MatchPerformative(ACLMessage.PROPOSE),
                MessageTemplate.MatchConversationId(conversationID)
        );

        // Setup the TTL (replyByDate)
        Date ttl = new Date(new Date().getTime() + TTL);

        // Create the message
        ACLMessage helloMessage = new SimpleMessage(
                nearbyAgents,
                conversationID,
                ttl,
                "Hello",
                ACLMessage.INFORM
        );
        helloMessage.setSender(new AID(getAgent().getLocalName(), AID.ISLOCALNAME));

        // Send the hello message, wait until the TTL expired
        List<ACLMessage> replies = this.sendMessageAndWaitReplies(helloMessage, ackTemplate);

        // Is somebody here ?
        if (replies.size() == 0) {
            setFinished(true);
            setSuccess(true);
            return;
        }

        // Open each replies and process graphs
        for (ACLMessage reply : replies) {
            try {
                Graph peerGraph = (Graph) reply.getContentObject();
                getGraph().addInfo(peerGraph);
            } catch (UnreadableException e) {
                // Something went wrong while retrieving the graph, let's just ignore it.
                // We could contact the agent and inform it its graph is unreadable
            }
        }

        // Now we send our updated graph to each contacted agent
        List<String> agents = new ArrayList<>();
        for (ACLMessage reply : replies) {
            agents.add(reply.getSender().getLocalName());
        }

        // Setup the ttl
        ttl.setTime(new Date().getTime() + TTL);

        try {
            // Setup the reply
            ACLMessage graphMessage = new SimpleMessage(
                    agents,
                    conversationID,
                    ttl,
                    getAgent().getGraph(),
                    ACLMessage.PROPOSE
            );
            graphMessage.setSender(new AID(getAgent().getLocalName(), AID.ISLOCALNAME));

            // Send the message
            getAgent().sendMessage(graphMessage);

            // Inform parent that we succeeded
            setSuccess(true);

        } catch (IOException e) {
            // Something went wrong, at least contact others to inform them
            ACLMessage errorMessage = new SimpleMessage(
                    agents,
                    conversationID,
                    ttl,
                    e.getMessage(),
                    ACLMessage.FAILURE
            );
            errorMessage.setSender(new AID(getAgent().getLocalName(), AID.ISLOCALNAME));
            
            //Send the message
            getAgent().sendMessage(errorMessage);
            setSuccess(false);

        } finally {
            setFinished(true);
        }
    }
}
