package mas.behaviours;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class SimpleMessage extends PirateMessage {

    public SimpleMessage(List<String> agents, String conversationID, Date ttl, String content, int performative) {
        super(agents, conversationID, ttl, performative);
        this.setContent(content);
    }

    public SimpleMessage(List<String> agents, String conversationID, int ttl, String content, int performative) {
        super(agents, conversationID, ttl, performative);
        this.setContent(content);
    }

    public <T extends Serializable> SimpleMessage(List<String> agents, String conversationID, Date ttl, T content, int performative) throws IOException {
        super(agents, conversationID, ttl, performative);
        this.setContentObject(content);
    }

    public <T extends Serializable> SimpleMessage(List<String> agents, String conversationID, int ttl, T content, int performative) throws IOException {
        super(agents, conversationID, ttl, performative);
        this.setContentObject(content);
    }

}
