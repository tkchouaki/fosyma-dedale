package mas.behaviours;

import graph.Graph;
import graph.Vertex;
import mas.agents.PirateAgent;
import util.Util;

import java.util.*;

/**
 * This class describes the strategy followed by the steward agent
 *      -The agent acts like a navigator until the map is explored
 *      -And then it goes to the central point of the map
 * On each moving step, the agent will observe the current vertex and then try to exchange his graph
 */
public class StewardStrategyBehaviour extends StrategyBehaviour {
    public StewardStrategyBehaviour(PirateAgent myAgent) {
        super(myAgent);
    }

    /**
     * after every move, the agent will observe the current vertex and then try to exchange his graph
     * @param target
     * The target vertex
     * @return
     * a list of behaviours
     */
    @Override
    public List<PirateBehaviour> generateStep(Vertex target) {
        List<PirateBehaviour> result = generateStep();
        PirateAgent agent = (PirateAgent) getAgent();
        result.add(0, new MoveBehaviour(agent, this, target.getId()));
        return result;
    }

    /**
     * at each step, the agent will try to exchange his graph
     * @return
     * a list of behaviours
     */
    @Override
    public List<PirateBehaviour> generateStep() {
        PirateAgent agent = (PirateAgent) getAgent();

        List<PirateBehaviour> step = new ArrayList<>();
        step.add(new SendGraphBehaviour(agent, this));
        step.add(new ReceiveGraphBehaviour(agent, this));

        Collections.shuffle(step);
        step.add(0, new ObserveBehaviour(agent, this));

        return step;
    }

    @Override
    public void action()
    {
        super.action();
    }

    /**
     * if the map is not fully discovered yet, the Steward Agent will act like a navigator agent
     * otherwise, it will go to the central point of the graph
     * @return
     * A set of targets
     */
    @Override
    public Set<Vertex> getTargets() {
        PirateAgent agent = getAgent();
        Graph graph = agent.getGraph();
        Vertex currentPosition = graph.getCurrentPosition();
        Set<Vertex> targets = new HashSet<>(graph.getDiscoveredVertices());
        Set<Vertex> temp;
        if (targets.size() == 0) {
            Vertex centralPoint = graph.getCentralPoint();
            if(centralPoint != null && !centralPoint.equals(currentPosition))
            {
                targets.add(centralPoint);
            }
        }
        else
        {
            temp = graph.getVerticesImClosestTo(targets);
            if(temp.size() != 0)
            {
                targets = temp;
            }
        }
        return targets;
    }
}
