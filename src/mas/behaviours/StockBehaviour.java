package mas.behaviours;

import mas.agents.PirateAgent;

import java.util.List;

public class StockBehaviour extends OnSpotBehaviour{

    public StockBehaviour(PirateAgent myAgent, PirateBehaviour parent) {
        super(myAgent, parent);
    }

    @Override
    public void action() {
        PirateAgent myAgent = this.getAgent();
        List<String> fellowAgents = this.getFellowAgentsList("silo");
        int i;
        for(i=0; i<fellowAgents.size() && !myAgent.emptyMyBackPack(fellowAgents.get(i)); i++);
        if(i<fellowAgents.size())
        {
            this.setSuccess(true);
            myAgent.getGraph().emptyTreasure();
        }
        else
        {
            this.setSuccess(false);
        }
        this.setFinished(true);
    }
}
