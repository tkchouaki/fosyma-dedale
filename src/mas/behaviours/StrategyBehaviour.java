package mas.behaviours;

import graph.Couple;
import graph.Graph;
import graph.Vertex;
import jade.core.AID;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import log.Log;
import mas.agents.PirateAgent;
import util.Util;

import java.io.IOException;
import java.util.*;

/**
 * The StrategyBehaviour is an abstract class designed to be a skeleton for other strategies. It should be seen as a
 * Scheduler, computing the next behaviour that should be run.
 * It comes with several utility methods, like ones that can take care of blocking.
 */
abstract class StrategyBehaviour extends PirateBehaviour {

    private static final int NB_SUCCESSES_BEFORE_GRAPH_EXCHANGE=20;

    private List<PirateBehaviour> currentPlan;
    private PirateBehaviour lastBehaviour;
    private Date lastComputeDate;
    private Set<Vertex> lastComputedTargets;
    private boolean isFollowingInterlock;
    private boolean blocked;
    private int nbSuccessesSinceLastExchange;


    public StrategyBehaviour(PirateAgent myAgent) {
        super(myAgent);
        this.currentPlan = new ArrayList<>();
        this.lastComputeDate = new Date(0);
        currentPlan.addAll(generateStep());
        this.lastBehaviour = null;
        this.blocked = false;
        this.isFollowingInterlock = false;
        this.nbSuccessesSinceLastExchange = 0;
    }


    public StrategyBehaviour(PirateAgent myAgent, PirateBehaviour parent)
    {
        super(myAgent, parent);
        this.currentPlan = new ArrayList<>();
        this.lastComputeDate = new Date(0);
        currentPlan.addAll(generateStep());
        this.lastBehaviour = null;
        this.blocked = false;
    }


    public List<PirateBehaviour> getCurrentPlan() {
        return currentPlan;
    }

    @Override
    public void action()
    {
        if(this.lastBehaviour != null && !this.lastBehaviour.done())
        {
            Log.write(this.myAgent.getLocalName() + " strange indeed");
            return;
        }

        if(this.currentPlan.size() == 0)
        {
            this.isFollowingInterlock = false;
        }

        PirateAgent myAgent = getAgent();

        //writing current plan in Log
        StringBuilder str = new StringBuilder(myAgent.getLocalName() + " has this to do :");

        for(PirateBehaviour behaviour : currentPlan)
        {
            str.append(behaviour.toString()).append(", ");
        }
        Log.write(str.toString());
        //--FINISHED writing current plan in logS

        if(this.lastBehaviour != null && this.lastBehaviour.getSuccess())
        {
            Log.write(myAgent.getLocalName() + " did this successfully " + lastBehaviour.toString());
            if(this.lastBehaviour instanceof ReceiveGraphBehaviour || this.lastBehaviour instanceof SendGraphBehaviour)
            {
                this.nbSuccessesSinceLastExchange = 0;
            }
            else
            {
                this.nbSuccessesSinceLastExchange++;
            }
        }

        if(this.lastBehaviour != null && !this.lastBehaviour.getSuccess())
        {
            Log.write(myAgent.getLocalName() + " failed to do this " + lastBehaviour.toString() + " and is now on " + this.getGraph().getCurrentPosition().getId());
        }

        if(this.lastBehaviour != null && !this.lastBehaviour.getSuccess() && this.lastBehaviour instanceof MoveBehaviour)
        {
            if(!this.isFollowingInterlock)
            {
                currentPlan.clear();
            }
            this.blocked = true;
            takeCareOfInterlocks();
            if(this.blocked || this.currentPlan.size() == 0)
            {
                this.computePathAndSetPlans(true);
                this.blocked = false;
            }
            isFollowingInterlock = true;
        }
        else if(!this.blocked && !this.isFollowingInterlock && (shouldComputeNextPath() || this.currentPlan.size() ==0 || (this.lastBehaviour != null && this.lastBehaviour instanceof StockBehaviour && this.lastBehaviour.getSuccess())))
        {
            computePathAndSetPlans();
        }
        startNextBehaviour();
    }

    public void computePathAndSetPlans()
    {
        this.computePathAndSetPlans(false);
    }

    public void computePathAndSetPlans(boolean ignoreFirstAndRandomIfNecessary)
    {
        Log.write(this.myAgent.getLocalName() + " computing path and setting plans");
        this.lastComputeDate = new Date();
        Set<Vertex> targets = new HashSet<>(this.getTargets());
        this.lastComputedTargets = targets;
        List<Vertex> path = this.getGraph().getPath(this.getGraph().getCurrentPosition(), targets);
        if(path.size()>0)
        {
            if(ignoreFirstAndRandomIfNecessary)
            {
                targets.remove(path.get(path.size()-1));
                path = this.getGraph().getPath(this.getGraph().getCurrentPosition(), targets);
                if(path.size()>0)
                {
                    path.remove(0);
                }
                else
                {
                    path = new ArrayList<>();
                    int nbTrials = 10;
                    Set<Vertex> forbidden = getGraph().getVerticesWithAgents();
                    forbidden.remove(getGraph().getCurrentPosition());
                    while(nbTrials>0 && path.size() == 0)
                    {
                        targets.clear();
                        targets.add((Vertex) Util.randomSetPick(getGraph().getVertices()));
                        path = getGraph().getPath(getGraph().getCurrentPosition(), targets);
                        nbTrials--;
                    }
                    if(path.size()>0)
                    {
                        System.out.println(this.myAgent.getLocalName()+" going randomly to " + path.get(path.size()-1));
                        path.remove(0);
                    }
                }
            }
            else
            {
                path.remove(0);
            }
        }
        currentPlan.clear();
        for(Vertex v : path)
        {
            currentPlan.addAll(generateStep(v));
        }
        if(path.size() == 0)
        {
            currentPlan.addAll(generateStep());
        }
    }


    public void startNextBehaviour()
    {
        if(this.currentPlan.size() > 0)
        {
            if(this.lastComputedTargets!= null && this.getCurrentPath().size() == 0 && this.lastComputedTargets.size() == 0)
            {
                //if the agent has nothing to do, he will at least listen to the interlocks messages
                Log.write(this.myAgent.getLocalName() + " has nothing to do");
                this.blocked = true;
                this.takeCareOfInterlocks(true);
                if(this.currentPlan.size() != 0)
                {
                    this.isFollowingInterlock = true;
                }
                this.blocked = false;
            }
            if(this.currentPlan.size() > 0)
            {
                while(this.nbSuccessesSinceLastExchange <= NB_SUCCESSES_BEFORE_GRAPH_EXCHANGE && this.currentPlan.size() >0 && (this.currentPlan.get(0) instanceof ReceiveGraphBehaviour || this.currentPlan.get(0) instanceof SendGraphBehaviour))
                {
                    this.currentPlan.remove(0);
                }
                if(this.currentPlan.size()>0)
                {
                    this.lastBehaviour = this.currentPlan.get(0);
                    Log.write(this.myAgent.getLocalName()+" is going to start " + this.currentPlan.get(0).toString());
                    this.myAgent.addBehaviour(this.currentPlan.remove(0));
                    this.block();
                }
            }
        }
        else
        {
            Log.write(this.myAgent.getLocalName() + " will take care of interlocks because i have nothing else to do");
            this.getTargets();
            takeCareOfInterlocks(true);
            //this.setFinished(true);
        }
    }

    //abstract public List<Vertex> computeNextPath();

    /**
     * This abstract method is intended to be defined by non-abstract subclasses
     * it generates a subplan for moving to a vertex
     * The subplan is generally composed of Moving-Observing-Communicating (SendGraph + ReceiveGraph)
     * @param target
     * The target vertex
     * @return
     * a list of behaviours that compose the subplan
     */
    abstract public List<PirateBehaviour> generateStep(Vertex target);

    /**
     * This abstract method is intended to be defined by non-abstract subclasses
     * It generates a subplan for staying on spot
     * @return
     * a list of behaviours that compose the subplan
     */
    abstract public List<PirateBehaviour> generateStep();


    public boolean shouldComputeNextPath()
    {
        PirateAgent myAgent = (PirateAgent) this.myAgent;
        if(currentPlan.size() == 0 || !(currentPlan.get(0) instanceof OnSpotBehaviour))
        {
            if(currentPlan.size() == 0 || myAgent.getGraph().getLastUpdate().getTime() > this.lastComputeDate.getTime())
            {
                return true;
            }
        }
        if(this.nbSuccessesSinceLastExchange <= NB_SUCCESSES_BEFORE_GRAPH_EXCHANGE)
        {
            boolean result = true;
            for(PirateBehaviour behaviour : currentPlan)
            {
                if(! (behaviour instanceof ReceiveGraphBehaviour) && ! (behaviour instanceof SendGraphBehaviour))
                {
                    result = false;
                }
            }
            return result;
        }
        return false;
    }

    protected Date getLastComputeDate()
    {
        return lastComputeDate;
    }

    protected PirateBehaviour getLastBehaviour()
    {
        return lastBehaviour;
    }

    /**
     * This method returns the path that is currently followed by the strategy
     * @return
     * A List of vertices ids representing the current path followed by the strategy, the first element is the agent's current position
     */
    protected List<String> getCurrentPath()
    {
        ArrayList<String> result = new ArrayList<>();
        PirateAgent myAgent = (PirateAgent) this.myAgent;
        Graph g = myAgent.getGraph();
        for(PirateBehaviour behaviour : this.currentPlan)
        {
            if(behaviour instanceof MoveBehaviour)
            {
                MoveBehaviour moveBehaviour = (MoveBehaviour) behaviour;
                result.add(moveBehaviour.getTarget());
            }
        }
        return result;
    }



    private void takeCareOfInterlocks()
    {
        this.takeCareOfInterlocks(false);
    }

    private void takeCareOfInterlocks(boolean slaveOnly)
    {
        if(!slaveOnly)
        {
            Log.write(this.myAgent.getLocalName() + " will try to solve an interlock");
        }


        MessageTemplate helloBlockingTemplate = MessageTemplate.and(
                MessageTemplate.MatchPerformative(ACLMessage.REQUEST),
                MessageTemplate.MatchContent(this.getGraph().getCurrentPosition().getId()));

        MessageTemplate meTooBlockingTemplate = MessageTemplate.MatchPerformative(ACLMessage.CONFIRM);


        this.emptyMail(helloBlockingTemplate);
        this.emptyMail(meTooBlockingTemplate);

        int minWaitingTime = 200;
        int maxWaitingIncrement = 50;
        SimpleMessage toSend;
        ACLMessage received;
        int i;

        for(i=0; i<2 && this.blocked; i++)
        {
            boolean shouldIWaitFirst = new Random().nextBoolean();
            if(shouldIWaitFirst || slaveOnly)
            {
                Log.write(this.myAgent.getLocalName() + " waits for a blocked");
                received = this.myAgent.blockingReceive(helloBlockingTemplate, minWaitingTime + new Random().nextInt()%maxWaitingIncrement);
                if(received != null)
                {
                    Log.write(this.myAgent.getLocalName() + " received a blocked and going to send a me too");
                    this.emptyMail(helloBlockingTemplate);
                    this.emptyMail(meTooBlockingTemplate);
                    toSend = new SimpleMessage(new ArrayList<>(), received.getConversationId(), 500, "me too", ACLMessage.CONFIRM);
                    toSend.setSender(this.myAgent.getAID());
                    toSend.addReceiver(received.getSender());
                    ((PirateAgent)this.myAgent).sendMessage(toSend);
                    this.takeCareOfInterlocksAsSlave(received.getSender(), received.getConversationId());
                }
                else
                {
                    if(!slaveOnly)
                    {
                        String myTargetVertex = "";
                        if(this.lastBehaviour instanceof MoveBehaviour)
                        {
                            myTargetVertex = ((MoveBehaviour) this.lastBehaviour).getTarget();
                        }
                        Log.write(this.myAgent.getLocalName() + " did not receive a blocked so going to send one");
                        String conversationID = "blocked" + new Random().nextInt();
                        toSend = new SimpleMessage(this.getFellowAgentsList(), conversationID, 500, myTargetVertex, ACLMessage.REQUEST);
                        toSend.setSender(this.myAgent.getAID());
                        ((PirateAgent)this.myAgent).sendMessage(toSend);
                        ACLMessage reply = this.myAgent.blockingReceive(MessageTemplate.and(meTooBlockingTemplate, MessageTemplate.MatchConversationId(conversationID)), minWaitingTime + new Random().nextInt(maxWaitingIncrement));
                        if(reply != null)
                        {
                            Log.write(this.myAgent.getLocalName() +  " received a me too");
                            this.takeCareOfInterlocksAsMaster(reply.getSender(), reply.getConversationId());
                        }
                    }
                }
            }
            else
            {
                String myTargetVertex = "";
                if(this.lastBehaviour instanceof MoveBehaviour)
                {
                    myTargetVertex = ((MoveBehaviour) this.lastBehaviour).getTarget();
                }
                Log.write(this.myAgent.getLocalName() + " going to send a blocked");
                String conversationID = "blocked" + new Random().nextInt();
                toSend = new SimpleMessage(this.getFellowAgentsList(), conversationID, 500, myTargetVertex, ACLMessage.REQUEST);
                toSend.setSender(this.myAgent.getAID());
                ((PirateAgent)this.myAgent).sendMessage(toSend);
                ACLMessage reply = this.myAgent.blockingReceive(MessageTemplate.and(meTooBlockingTemplate, MessageTemplate.MatchConversationId(conversationID)), minWaitingTime + new Random().nextInt(maxWaitingIncrement));
                if(reply != null)
                {
                    Log.write(this.myAgent.getLocalName() + " received a me too");
                    this.takeCareOfInterlocksAsMaster(reply.getSender(), reply.getConversationId());
                }
                else
                {
                    Log.write(this.myAgent.getLocalName() + " did not receive a mee too so going to wait for a blocked");
                    received = this.myAgent.blockingReceive(helloBlockingTemplate, minWaitingTime + new Random().nextInt()%maxWaitingIncrement);
                    this.emptyMail(meTooBlockingTemplate);
                    this.emptyMail(helloBlockingTemplate);
                    if(received != null)
                    {
                        this.emptyMail(helloBlockingTemplate);
                        this.emptyMail(meTooBlockingTemplate);
                        toSend = new SimpleMessage(new ArrayList<>(), received.getConversationId(), 500, "", ACLMessage.CONFIRM);
                        toSend.setSender(this.myAgent.getAID());
                        toSend.addReceiver(received.getSender());
                        ((PirateAgent)this.myAgent).sendMessage(toSend);
                        Log.write(this.myAgent.getLocalName() + " received a blocked and sent a me too");
                        this.takeCareOfInterlocksAsSlave(received.getSender(), received.getConversationId());
                    }
                }
            }
        }
    }

    /**
     * This method solves the interlocks from a master point of view
     * @param slave
     * The AID of the slave
     * @param conversationId
     * The conversationID of the initiated protocl
     */
    private void takeCareOfInterlocksAsMaster(AID slave, String conversationId)
    {
        Log.write(this.myAgent.getLocalName() + " will try to solve the interlock as a master with " + slave.getLocalName());
        int minWaitingTime = 200;
        int maxWaitingIncrement = 50;

        //The master agent first sends its graph to the slave agent
        SimpleMessage grapheMessage;
        try {
            grapheMessage = new SimpleMessage(new ArrayList<>(), conversationId, 500, this.getGraph(), ACLMessage.NOT_UNDERSTOOD);
        } catch (IOException e) {
            return;
        }

        grapheMessage.setSender(this.myAgent.getAID());
        grapheMessage.addReceiver(slave);


        ((PirateAgent)this.myAgent).sendMessage(grapheMessage);

        //After adding his graph to the master's graph, the slave agent replies with the resulting graph & its targets
        ACLMessage infoMessage = this.myAgent.blockingReceive(MessageTemplate.MatchConversationId(conversationId), minWaitingTime + new Random().nextInt(maxWaitingIncrement));


        if(infoMessage!=null)
        {
            long oldTime = new Date().getTime();
            try
            {
                //the master retrieves the info sent by the slave
                Couple<Graph, Set<Vertex>> info = (Couple<Graph, Set<Vertex>>) infoMessage.getContentObject();
                Graph slaveGraph = info.getX();
                //the slave's graph is added to the master's one
                this.getGraph().addInfo(slaveGraph);

                //the master updates its targets (they may have changed since the graph may have too)
                Set<Vertex> masterTargets = new HashSet<>();
                if(this.isFollowingInterlock)
                {
                    List<String> currentPath = this.getCurrentPath();
                    if(currentPath.size() > 0)
                    {
                        masterTargets.add(new Vertex(currentPath.get(currentPath.size()-1)));
                    }
                }
                if(masterTargets.size()==0)
                {
                    masterTargets = this.getTargets();
                }
                Set<Vertex> slaveTargets = info.getY();

                Log.write(myAgent.getLocalName() + " wishes to go from " + this.getGraph().getCurrentPosition().getId() + " to " + masterTargets.toString());
                Log.write(slave.getLocalName() + " wishes to go from " + slaveGraph.getCurrentPosition().getId()+ " to " + slaveTargets.toString());


                //we verify first that using the new graph & targets that each agent have, there's still an intlock problem
                List<Vertex> masterOriginalPath = this.getGraph().getPath(this.getGraph().getCurrentPosition(), masterTargets);
                List<Vertex> slaveOriginalPath = this.getGraph().getPath(slaveGraph.getCurrentPosition(), slaveTargets);

                List<Vertex> pathToFollow=null;
                List<Vertex> pathToSend=null;

                boolean isInterblocked = true;
                if(masterOriginalPath.size() > 0 && slaveOriginalPath.size() >0)
                {
                    masterOriginalPath.remove(0);
                    slaveOriginalPath.remove(0);
                    if(!masterOriginalPath.contains(slaveGraph.getCurrentPosition()) || !slaveOriginalPath.contains(this.getGraph().getCurrentPosition()))
                    {
                        pathToFollow = masterOriginalPath;
                        pathToSend = slaveOriginalPath;
                        isInterblocked = false;
                    }
                }
                //if there's still an interlock problem
                if(isInterblocked)
                {

                    HashMap<String, Couple<Vertex, Set<Vertex>>> entry = new HashMap<>();
                    entry.put(this.myAgent.getLocalName(), new Couple<>(this.getGraph().getCurrentPosition(), masterTargets));
                    entry.put(slave.getLocalName(), new Couple<>(info.getX().getCurrentPosition(), info.getY()));

                    if(slaveTargets.size() == 0)
                    {
                        slaveTargets.addAll(getGraph().getVertices());
                    }

                    //we try to resolve the problem by setting the master as primary & the slave as secondary
                    HashMap<String, List<Vertex>> paths = this.getGraph().getPathsForTwoInterlockedAgents(entry, this.myAgent.getLocalName());

                    //if it does not succeed, we invert the priorities
                    if(paths == null) paths = this.getGraph().getPathsForTwoInterlockedAgents(entry, slave.getLocalName());

                    if(paths != null)
                    {
                        pathToFollow = paths.get(this.myAgent.getLocalName());
                        pathToSend = paths.get(slave.getLocalName());
                        pathToFollow.remove(0);
                        pathToSend.remove(0);

                        if(pathToFollow.size() == 0 || pathToSend.size() == 0)
                        {
                            if(masterOriginalPath.size() > 0 && slaveOriginalPath.size() >0)
                            {
                                masterOriginalPath.remove(0);
                                slaveOriginalPath.remove(0);
                                if(!masterOriginalPath.contains(slaveGraph.getCurrentPosition()) || !slaveOriginalPath.contains(this.getGraph().getCurrentPosition()))
                                {
                                    pathToFollow = masterOriginalPath;
                                    pathToSend = slaveOriginalPath;
                                    isInterblocked = false;
                                }
                            }
                            paths = this.getGraph().getPathsForTwoInterlockedAgents(entry, this.myAgent.getLocalName());

                            if(paths == null) paths = this.getGraph().getPathsForTwoInterlockedAgents(entry, slave.getLocalName());
                        }
                    }
                    else
                    {
                        paths = this.getGraph().getPathsForTwoInterlockedAgents(entry, this.myAgent.getLocalName());

                        if(paths == null) paths = this.getGraph().getPathsForTwoInterlockedAgents(entry, slave.getLocalName());
                    }
                }
                if(pathToFollow != null && pathToSend != null)
                {
                    Log.write(myAgent.getLocalName() + " will follow " + pathToFollow.toString());
                    Log.write(slave.getLocalName() + " will follow " + pathToFollow.toString());
                    Couple<Integer, List<Vertex>> toSend = new Couple<>(0, pathToSend);
                    ACLMessage message = new SimpleMessage(Arrays.asList(new String[]{slave.getLocalName()}), conversationId, 500, toSend, ACLMessage.INFORM);
                    message.setSender(this.myAgent.getAID());
                    ((PirateAgent) this.myAgent).sendMessage(message);
                    currentPlan.clear();
                    for(Vertex v: pathToFollow)
                    {
                        currentPlan.addAll(generateStep(v));
                    }
                    this.blocked = false;
                }
                this.emptyMail();
            } catch (UnreadableException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    private void takeCareOfInterlocksAsSlave(AID master, String conversationId)
    {
        Log.write(this.myAgent.getLocalName() + " will try to solve the interlock as a slave");
        MessageTemplate blockingMessageTemplate = MessageTemplate.MatchPerformative(ACLMessage.NOT_UNDERSTOOD);
        int minWaitingTime = 200;
        int maxWaitingIncrement = 50;


        ACLMessage graphMessage = this.myAgent.blockingReceive(MessageTemplate.MatchConversationId(conversationId), minWaitingTime + new Random().nextInt(maxWaitingIncrement));

        if(graphMessage == null) { return; }

        Graph masterGraphe;

        try {
            masterGraphe = (Graph) graphMessage.getContentObject();
        } catch (UnreadableException e) {
            return;
        }

        this.getGraph().addInfo(masterGraphe);

        SimpleMessage infoMessage;

        Set<Vertex> targets = new HashSet<>();
        if(this.isFollowingInterlock)
        {
            List<String> currentPath = this.getCurrentPath();
            if(currentPath.size()>0)
            {
                targets.add(new Vertex(currentPath.get(currentPath.size()-1)));
            }
        }
        if(targets.size()==0)
        {
            targets = this.getTargets();
        }

        try {
            infoMessage = new SimpleMessage(new ArrayList<>(), conversationId, 500, new Couple<Graph, Set<Vertex>>(this.getGraph(), targets), ACLMessage.NOT_UNDERSTOOD);
        } catch (IOException e) {
           return;
        }
        infoMessage.addReceiver(master);
        infoMessage.setSender(this.myAgent.getAID());
        ((PirateAgent) this.myAgent).sendMessage(infoMessage);


        ACLMessage instructionMessage = this.myAgent.blockingReceive(MessageTemplate.MatchConversationId(conversationId), minWaitingTime + new Random().nextInt(maxWaitingIncrement));

        if(instructionMessage != null)
        {
            List<Vertex> instructions;
            Couple<Integer, List<Vertex>> content;
            try {
                content = (Couple<Integer, List<Vertex>>) instructionMessage.getContentObject();
            } catch (UnreadableException e) {
                return;
            }
            currentPlan.clear();
            instructions = content.getY();
            for(Vertex v : instructions)
            {
                currentPlan.addAll(generateStep(v));
            }
            this.blocked = false;
            this.isFollowingInterlock = true;
        }
    }

    abstract public Set<Vertex> getTargets();
}
